using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using NE_BaseObject;
using NE_BaseObject.Admin;
using NE_BaseObject.Api;
using NE_DA.Admin;
using NE_DA.Provider;
using NE_DA_Interface.Admin;
using NE_DA_Interface.Provider;
using NE_Mgr.Admin;
using NE_Mgr.Provider;
using NE_Mgr_Interface.Admin;
using NE_Mgr_Interface.Provider;
using NearByExperiences.FileUpload;
using NearByExperiences.Helpers;

namespace NearByExperiences
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRazorPages();
            services.AddServerSideBlazor();

            services.AddTransient<IAdminMgr, AdminMgr>();
            services.AddTransient<IAdminDA, AdminDA>();

            services.AddTransient<IProviderMgr, ProviderMgr>();
            services.AddTransient<IProviderDA, ProviderDA>();

            services.AddTransient<IHttpContextAccessor, HttpContextAccessor>();
            //services.AddHttpContextAccessor();
            //services.AddDistributedMemoryCache();

            services.AddControllers();
            services.AddScoped<IFileUploadData, FileUploadData>();

            services.AddMvc();
            services.AddMvc().AddSessionStateTempDataProvider();
            //services.AddMvc().AddNewtonsoftJson();

            services.AddSession(options =>
            {
                // Set a short timeout for easy testing.
                options.IdleTimeout = TimeSpan.FromMinutes(30);
                options.Cookie.HttpOnly = true;
                // Make the session cookie essential
                options.Cookie.IsEssential = true;
            });
            services.AddSingleton<SessionState>();
            services.AddSingleton<SessionBootstrapper>();

            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<MappingProfile>();
            });
            services.AddAutoMapper(typeof(Startup));   //mandatory user for mapping in mgr to da
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/404");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseCors(builder => builder.AllowAnyHeader().AllowAnyOrigin().AllowAnyMethod());
            app.UseSession();
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseRouting();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapBlazorHub( o =>
                {
                    o.ApplicationMaxBufferSize = 92233720; // larger size
                    o.TransportMaxBufferSize = 92233720; // larger size
                });
                endpoints.MapFallbackToPage("/_Host");
            });
        }

        public class MappingProfile : Profile
        {
            public MappingProfile()
            {
                CreateMap<ResultObject<AdminUserData>, ResultObject<AdminUserData>>();
                CreateMap<AdminUserData, AdminUserData>();

                CreateMap<ResultObject<List<CityMasterData>>, ResultObject<List<CityMasterData>>>();
                CreateMap<CityMasterData, CityMasterData>();

                CreateMap<ResultObject<string>, ResultObject<string>>();

                CreateMap<ResultObject<List<TagsMasterData>>, ResultObject<List<TagsMasterData>>>();
                CreateMap<CityMasterData, TagsMasterData>();

                CreateMap<ResultObject<List<CmsPageData>>, ResultObject<List<CmsPageData>>>();
                CreateMap<CmsPageData, CmsPageData>();

                CreateMap<ResultObject<List<SignupRequestData>>, ResultObject<List<SignupRequestData>>>();
                CreateMap<SignupRequestData, SignupRequestData>();

                CreateMap<ResultObject<List<ActivityProviderData>>, ResultObject<List<ActivityProviderData>>>();
                CreateMap<ActivityProviderData, ActivityProviderData>();

                CreateMap<ResultObject<List<ActivityMasterData>>, ResultObject<List<ActivityMasterData>>>();
                CreateMap<ActivityMasterData, ActivityMasterData>();

                CreateMap<ResultObject<List<ProviderData>>, ResultObject<List<ProviderData>>>();
                CreateMap<ProviderData, ProviderData>();

                CreateMap<ResultObject<List<ActivitIncludeData>>, ResultObject<List<ActivitIncludeData>>>();
                CreateMap<ActivitIncludeData, ActivitIncludeData>();

                CreateMap<ResultObject<ProfileData>, ResultObject<ProfileData>>();
                CreateMap<ProfileData, ProfileData>();

                CreateMap<ChangePasswordData, ChangePasswordData>();
            }
        }
    }
}
