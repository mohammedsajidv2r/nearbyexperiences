﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using NE_BaseObject;
using NE_BaseObject.Admin;
using NE_Mgr_Interface.Admin;
using NE_Utilities;
using NE_Utilities.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NearByExperiences.Services
{
    public class LogIn
    {
        IConfiguration _iConfig;
        IHttpContextAccessor _httpContextAccessor;
        IAdminMgr _IAdminMgr;

        public LogIn(IConfiguration iConfig, IHttpContextAccessor httpContextAccessor, IAdminMgr IAdminMgr)
        {
            _iConfig = iConfig;
            _httpContextAccessor = httpContextAccessor;
            _IAdminMgr = IAdminMgr;
        }

        #region login
        public ResultObject<AdminUserData> adminLoginAuth(string RequestType, AdminUserData data)
        {
            ResultObject<AdminUserData> res = new ResultObject<AdminUserData>();
            try
            {
                res = _IAdminMgr.adminLoginAuth(RequestType, data);
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs("adminLoginAuth", "adminLoginAuth", "LogIn", 0, ex.Message.ToString());
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }

        public ResultObject<AdminUserData> checkLogin()
        {
            ResultObject<AdminUserData> loginChk = new ResultObject<AdminUserData>();
            try
            {
                loginChk.ResultData = new AdminUserData();
                loginChk.ResultData = _httpContextAccessor.HttpContext.Session.GetObject<AdminUserData>("AuthLoginData");
                loginChk.Result = ResultType.Success;
            }
            catch (Exception ex)
            {
                _httpContextAccessor.HttpContext.Session.Clear();
            }
            if (loginChk.ResultData == null)
            {
                loginChk.Result = ResultType.Error;
            }
            return loginChk;
        }

        public void logOut()
        {
            _httpContextAccessor.HttpContext.Session.Clear();
            //_httpContextAccessor.HttpContext.Response.Redirect("/"); 
        }

        #endregion

        #region cityMaster
        public ResultObject<List<CityMasterData>> cityMaster()
        {
            ResultObject<List<CityMasterData>> res = new ResultObject<List<CityMasterData>>();
            try
            {
                res = _IAdminMgr.cityMaster("viewcity", "", "");
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }

        public ResultObject<string> editCityMaster(string reqtype, string param, CityMasterData data)
        {
            ResultObject<string> res = new ResultObject<string>();
            try
            {
                res = _IAdminMgr.editCityMaster(reqtype, param, data);
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }

        public ResultObject<List<CityMasterData>> cityMasterId(string req, string param)
        {
            ResultObject<List<CityMasterData>> res = new ResultObject<List<CityMasterData>>();
            try
            {
                res = _IAdminMgr.cityMaster(req, param, "");
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }
        #endregion


        #region tagMaster
        public ResultObject<List<TagsMasterData>> tagMaster()
        {
            ResultObject<List<TagsMasterData>> res = new ResultObject<List<TagsMasterData>>();
            try
            {
                res = _IAdminMgr.tagMaster("viewtag", "", "");
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }

        public ResultObject<string> editTagMaster(string reqtype, string param, TagsMasterData data)
        {
            ResultObject<string> res = new ResultObject<string>();
            try
            {
                res = _IAdminMgr.editTagMaster(reqtype, param, data);
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }
        #endregion

        #region cmsMaster
        public ResultObject<List<CmsPageData>> cmsMaster()
        {
            ResultObject<List<CmsPageData>> res = new ResultObject<List<CmsPageData>>();
            try
            {
                res = _IAdminMgr.cmsMaster("viewcms", "", "");
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }

        public ResultObject<List<CmsPageData>> cmsMasterreq(string reqtype, string param)
        {
            ResultObject<List<CmsPageData>> res = new ResultObject<List<CmsPageData>>();
            try
            {
                res = _IAdminMgr.cmsMaster(reqtype, param, "");
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }


        public ResultObject<string> editCmsMaster(string reqtype, string param, CmsPageData data)
        {
            ResultObject<string> res = new ResultObject<string>();
            try
            {
                res = _IAdminMgr.editCmsMaster(reqtype, param, data);
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }
        #endregion

        #region signupReq
        public ResultObject<List<SignupRequestData>> signrequestMaster()
        {
            ResultObject<List<SignupRequestData>> res = new ResultObject<List<SignupRequestData>>();
            try
            {
                res = _IAdminMgr.signrequestMaster("viewsignupr", "", "");
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }

        public ResultObject<string> editSignMaster(string reqtype, string param, SignupRequestData data)
        {
            ResultObject<string> res = new ResultObject<string>();
            try
            {
                res = _IAdminMgr.editSignMaster(reqtype, param, data);
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }
        #endregion


        #region ActivityProvider
        public ResultObject<List<ActivityProviderData>> acitvityProvider(string req, string param)
        {
            ResultObject<List<ActivityProviderData>> res = new ResultObject<List<ActivityProviderData>>();
            try
            {
                res = _IAdminMgr.acitvityProvider(req, param, "");
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }

        public ResultObject<string> editacitvityProvider(string reqtype, string param, ActivityProviderData data)
        {
            ResultObject<string> res = new ResultObject<string>();
            try
            {
                res = _IAdminMgr.editacitvityProvider(reqtype, param, data);
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }

        public ResultObject<List<ActivityMasterData>> acitvityMaster(string req, string param)
        {
            ResultObject<List<ActivityMasterData>> res = new ResultObject<List<ActivityMasterData>>();
            try
            {
                res = _IAdminMgr.acitvityMaster(req, param, "");
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }

        public ResultObject<string> editacitvityMaster(string reqtype, string param, ActivityMasterData data)
        {
            ResultObject<string> res = new ResultObject<string>();
            try
            {
                res = _IAdminMgr.editacitvityMaster(reqtype, param, data);
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }

        public ResultObject<List<ActivitIncludeData>> activityInclude(string param)
        {
            ResultObject<List<ActivitIncludeData>> res = new ResultObject<List<ActivitIncludeData>>();
            try
            {
                res = _IAdminMgr.activityInclude("viewTblActivity", param, "");
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }

        public ResultObject<string> editacitvityMasterWithInclude(string reqtype, string param, ActivityMasterData data, ActivitIncludeData data1)
        {
            ResultObject<string> res = new ResultObject<string>();
            try
            {
                res = _IAdminMgr.editacitvityMasterWithInclude(reqtype, param, data, data1);
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }

        public ResultObject<string> editacitvityMasterWithIncludeList(string reqtype, string param, ActivityMasterData data, List<ActivitIncludeData> data1)
        {
            ResultObject<string> res = new ResultObject<string>();
            try
            {
                res = _IAdminMgr.editacitvityMasterWithIncludeList(reqtype, param, data, data1);
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }
        #endregion

        #region testimonilal
        public ResultObject<List<TestimonialData>> tetimonial()
        {
            ResultObject<List<TestimonialData>> res = new ResultObject<List<TestimonialData>>();
            try
            {
                res = _IAdminMgr.tetimonial("viewTestmonial", "", "");
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }

        public ResultObject<string> edittetimonial(string reqtype, string param, TestimonialData data)
        {
            ResultObject<string> res = new ResultObject<string>();
            try
            {
                res = _IAdminMgr.edittetimonial(reqtype, param, data);
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }
        #endregion

        #region profile
        public ResultObject<ProfileData> getProfileData(string RequestType, string param1, string param2)
        {
            ResultObject<ProfileData> res = new ResultObject<ProfileData>();
            try
            {
                res = _IAdminMgr.getProfileData(RequestType, param1, param2);
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs("adminLoginAuth", "getProfileData", "LogIn", 0, ex.Message.ToString());
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }

        public ResultObject<string> updateProfile(string reqtype, string param1, string param2, ProfileData data)
        {
            ResultObject<string> res = new ResultObject<string>();
            try
            {
                res = _IAdminMgr.updateProfile(reqtype, param1, param2, data);
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }
        public ResultObject<string> resetpassword(string email)
        {
            ResultObject<string> res = new ResultObject<string>();
            try
            {
                res = _IAdminMgr.resetpassword("resetpassword", email, "");
                if (res.Result == ResultType.Success)
                {
                    string idData = res.ResultData.ToString() + "/" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    //string idData = "1" + "/" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    string encrypPass = Encryption.EncryptString(idData, Encryption.passkey);
                    encrypPass = encrypPass.Replace('+', '_');
                    encrypPass = encrypPass.Replace('/', '~');
                    string url = $"{_httpContextAccessor.HttpContext.Request.Scheme}://{_httpContextAccessor.HttpContext.Request.Host}";

                    string body = "Hello admin!  <br/><br/>" +
                      "You are receiving this email because we recevied a password reset request for your account.<br/><br/>" +
                      "You are just one step away from reset your password.<br/>" +
                       //"Please click on the below link to reset the password.<br/>" +
                       url + "/resetpassword/" + encrypPass + "<br/>" +
                       "Thank you <br/>" +
                       "Team Nearby Experiences. <br/>" +
                       url + " <br/><br/>" +
                      "Copyright © 2020 Nearby Experiences, All rights reserved.";

                    string subject = "Reset Password Request";
                    //string check = SentEmail.emailSent(body, subject, SentEmail.fromemail, email);
                    string check = SentEmail.emailSentUat(body, subject, SentEmail.fromemailtest, email);
                    if (check == "1")
                    {
                        res.ResultMessage = "You will receive a password reset link at your e-mail address. Please click on that link to reset your password";
                    }
                    else
                    {
                        res.Result = ResultType.Info;
                        res.ResultMessage = "Mail Service not working.";
                    }
                }
                else
                {
                    res.Result = ResultType.Info;
                    res.ResultMessage = "User with this e-mail address is not registered with Nearby experiences";
                }
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }

        public string ForgotPass(string userID)
        {
            string uid = "error";
            try
            {
                string encryp = userID;
                userID = userID.Replace('_', '+');
                userID = userID.Replace('~', '/');
                uid = Encryption.DecryptString(userID, Encryption.passkey);
                string[] idData = uid.Split("/");
                DateTime genDate = DateTime.Parse(idData[1]);
                genDate = genDate.AddMinutes(30);
                if (genDate >= DateTime.Now)
                {
                    uid = "success";
                    ResultObject<string> res = new ResultObject<string>();
                    res = _IAdminMgr.updateresetpass("resetpasswordlinkcheck", "", encryp, idData[0]);
                    if (res.ResultData == "1")
                    {
                        uid = "success";
                    }
                    else
                    {
                        uid = "error";
                    }
                }
                else
                {
                    uid = "error";
                }
            }
            catch (Exception ex)
            {
                uid = "error";
            }
            return uid;
        }


        public ResultObject<string> updateresetpass(string reqtype, string param1, string param2, string  param3)
        {
            ResultObject<string> res = new ResultObject<string>();
            try
            {
                string encryp = param3;
                param3 = param3.Replace('_', '+');
                param3 = param3.Replace('~', '/');
                string uid = Encryption.DecryptString(param3, Encryption.passkey);
                string[] idData = uid.Split("/");
                param3 = idData[0];
                res = _IAdminMgr.updateresetpass(reqtype, param1, encryp, param3);
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }

        #endregion


        public ResultObject<List<ConfigurationData>> viewConfiguration()
        {
            ResultObject<List<ConfigurationData>> res = new ResultObject<List<ConfigurationData>>();
            try
            {
                res = _IAdminMgr.viewConfiguration("viewconfiguation", "", "");
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }


        public ResultObject<string> editConfiguration(ConfigurationData data)
        {
            ResultObject<string> res = new ResultObject<string>();
            try
            {
                res = _IAdminMgr.editConfiguration("updateconfiguration", "", data);
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }

        public ResultObject<DashboardData> dashboardData()
        {
            ResultObject<DashboardData> res = new ResultObject<DashboardData>();
            try
            {
                res = _IAdminMgr.dashboardData("viewdashboard", "", "");
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }


        #region faq
        public ResultObject<List<FaqData>> viewfaq(string req, string param)
        {
            ResultObject<List<FaqData>> res = new ResultObject<List<FaqData>>();
            try
            {
                res = _IAdminMgr.viewfaq(req, param, "");
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }

        public ResultObject<string> editfaq(string reqtype, string param, FaqData data)
        {
            ResultObject<string> res = new ResultObject<string>();
            try
            {
                res = _IAdminMgr.editfaq(reqtype, param, data);
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }
        #endregion
    }
}
