﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using NE_BaseObject;
using NE_BaseObject.Admin;
using NE_BaseObject.Api;
using NE_Mgr_Interface.Admin;
using NE_Mgr_Interface.Provider;
using NE_Utilities;
using NE_Utilities.Enum;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using static NE_BaseObject.PostModel.PostDataModel;

namespace NearByExperiences.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class NearbyPostController : ControllerBase
    {
        IConfiguration _iConfig;
        IHttpContextAccessor _httpContextAccessor;
        IAdminMgr _IAdminMgr;
        IProviderMgr _IProviderMgr;

        public NearbyPostController(IAdminMgr IAdminMgr, IConfiguration iConfig, IHttpContextAccessor httpContextAccessor, IProviderMgr IProviderMgr)
        {
            _iConfig = iConfig;
            _httpContextAccessor = httpContextAccessor;
            _IAdminMgr = IAdminMgr;
            _IProviderMgr = IProviderMgr;
        }

        [HttpPost]
        public string encryption(EncryptPostData data)
        {
            byte[] encrypted = AESEncryption.EncryptStringToBytes(data.hdndata);
            return Convert.ToBase64String(encrypted.ToArray());

            //Convert.ToBase64String(AESEncryption.EncryptStringToBytes());
        }

        [HttpPost]
        public string decryption(EncryptPostData data)
        {
            string encrypt = AESEncryption.DecryptStringAES(data.hdndata);
            expolorActivity json = JsonConvert.DeserializeObject<expolorActivity>(encrypt);
            return encrypt;
        }

        [HttpPost]
        //public dynamic getExploreActivity(expolorActivity data)
        public dynamic getExploreActivity(EncryptPostData encryptdata)
        {
            try
            {
                string decryptdata = AESEncryption.DecryptStringAES(encryptdata.hdndata);
                expolorActivity data = JsonConvert.DeserializeObject<expolorActivity>(decryptdata);
                ResultObject<List<ProviderData>> res = _IProviderMgr.getProvider("viewExplorActivity", data.slugKey, "");
                if (res.Result == ResultType.Success)
                {
                    string imgurl = $"{_httpContextAccessor.HttpContext.Request.Scheme}://{_httpContextAccessor.HttpContext.Request.Host}";
                    imgurl = imgurl + "/images/thumb/";

                    foreach (ProviderData sd in res.ResultData)
                    {
                        if (sd.providerpicture == "" || sd.providerpicture == null)
                        {
                            sd.providerpicture = "";
                        }
                        else
                        {
                            sd.providerpicture = imgurl + sd.providerpicture;
                        }

                        if (sd.profilepicture == "" || sd.profilepicture == null)
                        {
                            sd.profilepicture = "";
                        }
                        else
                        {
                            sd.profilepicture = imgurl + sd.profilepicture;
                        }
                    }

                    return new
                    {
                        result = 1,
                        data = res.ResultData.Select(x => new
                        {
                            activity_id = x.activity_master_id,
                            provider_id = x.activity_provider_id,
                            x.first_name,
                            x.last_name,
                            x.location,
                            activity_title = x.title,
                            activity_price = x.price,
                            x.short_description,
                            activity_picture = x.providerpicture,
                            provider_profile_picture = x.profilepicture,
                            x.slug_url,
                            x.star_rating,
                            x.provider_slug_url,
                            x.profession
                        }),
                    } as dynamic;
                }
                else
                {
                    return new { result = 0, data = "" } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new { result = 0, data = "" } as dynamic;
            }
        }

        [HttpPost]
        //public dynamic getActivityDetails(activity data)
        public dynamic getActivityDetails(EncryptPostData encryptdata)
        {
            try
            {
                string decryptdata = AESEncryption.DecryptStringAES(encryptdata.hdndata);
                activity data = JsonConvert.DeserializeObject<activity>(decryptdata);
                ResultObject<List<ProviderData>> res = _IProviderMgr.getProvider("viewActivityDetails", data.slugKey, "");
                ResultObject<List<ActivitIncludeData>> resInc = _IAdminMgr.activityInclude("viewTblActivity", res.ResultData.Select(x => x.activity_master_id).FirstOrDefault().ToString(), "");

                ResultObject<List<ProviderData>> resSimilar = _IProviderMgr.getProvider("viewsimilarpost", res.ResultData.Select(x => x.city_id).FirstOrDefault(), "");
                if (res.Result == ResultType.Success)
                {

                    //treksoft
                    if (res.ResultData.Select(x => x.widget_type).FirstOrDefault() == 1)
                    {
                        try
                        {
                            string wid = res.ResultData.Select(x => x.widget_code).FirstOrDefault().ToString();
                            dynamic json = JsonConvert.DeserializeObject(wid);
                            string widurl = json.ToString();
                            res.ResultData.Select(s => { s.widget_code = widurl; return widurl; }).ToList();
                        }
                        catch (Exception ex)
                        {

                        }

                    }
                    string imgurl = $"{_httpContextAccessor.HttpContext.Request.Scheme}://{_httpContextAccessor.HttpContext.Request.Host}";
                    imgurl = imgurl + "/images/";

                    string thumurl = imgurl + "thumb/";

                    foreach (ProviderData sd in res.ResultData)
                    {
                        if (sd.providerpicture == "" || sd.providerpicture == null)
                        {
                            sd.providerpicture = "";
                        }
                        else
                        {
                            sd.providerpicture = imgurl + sd.providerpicture;
                        }

                        if (sd.profilepicture == "" || sd.profilepicture == null)
                        {
                            sd.profilepicture = "";
                        }
                        else
                        {
                            sd.profilepicture = imgurl + sd.profilepicture;
                        }
                    }
                    foreach (ProviderData sd in resSimilar.ResultData)
                    {
                        if (sd.providerpicture == "" || sd.providerpicture == null)
                        {
                            sd.providerpicture = "";
                        }
                        else
                        {
                            sd.providerpicture = thumurl + sd.providerpicture;
                        }

                        if (sd.profilepicture == "" || sd.profilepicture == null)
                        {
                            sd.profilepicture = "";
                        }
                        else
                        {
                            sd.profilepicture = thumurl + sd.profilepicture;
                        }
                    }
                    return new
                    {
                        result = 1,
                        data = res.ResultData.Select(x => new
                        {
                            activity_id = x.activity_master_id,
                            provider_id = x.activity_provider_id,
                            x.first_name,
                            x.last_name,
                            x.location,
                            activity_title = x.title,
                            x.description,
                            activity_price = x.price,
                            activity_picture = x.providerpicture,
                            provider_profile_picture = x.profilepicture,
                            x.highlight,
                            x.star_rating,
                            x.short_description,
                            x.tags_name,
                            x.slug_url,
                            x.city_slug_url,
                            x.provider_slug_url,
                            x.widget_type,
                            widget_url = x.widget_code,
                            donation_text = x.donation
                        }).FirstOrDefault(),
                        dataInclude = resInc.ResultData.Select(x => new { x.include_desc, x.include_id, x.active }),
                        dataSimilar = resSimilar.ResultData.Where(x => x.activity_master_id != res.ResultData.Select(x => x.activity_master_id).FirstOrDefault()).Select(x => new
                        {
                            activity_id = x.activity_master_id,
                            provider_id = x.activity_provider_id,
                            x.first_name,
                            x.last_name,
                            x.location,
                            activity_title = x.title,
                            x.short_description,
                            activity_price = x.price,
                            activity_picture = x.providerpicture,
                            provider_profile_picture = x.profilepicture,
                            x.slug_url,
                            x.provider_slug_url
                        }),
                    } as dynamic;
                }
                else
                {
                    return new { result = 0, data = "" } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new { result = 0, data = "" } as dynamic;
            }
        }

        [HttpPost]
        //public dynamic getProviderDetails(activity data)
        public dynamic getProviderDetails(EncryptPostData encryptdata)
        {
            try
            {
                string decryptdata = AESEncryption.DecryptStringAES(encryptdata.hdndata);
                activity data = JsonConvert.DeserializeObject<activity>(decryptdata);
                ResultObject<List<ActivityProviderData>> res = _IAdminMgr.acitvityProvider("viewactivityProviderbyidApi", data.slugKey, "");
                if (res.Result == ResultType.Success)
                {
                    ResultObject<List<ProviderData>> resAct = _IProviderMgr.getProvider("viewActivityByProvider", res.ResultData.Select(x => x.activity_provider_id).FirstOrDefault().ToString(), "");
                    ResultObject<List<ActivityProviderData>> resProf = _IAdminMgr.acitvityProvider("viewProviderbyidApi", data.slugKey, "");
                    string imgurl = $"{_httpContextAccessor.HttpContext.Request.Scheme}://{_httpContextAccessor.HttpContext.Request.Host}";
                    imgurl = imgurl + "/images/";

                    string thumurl = imgurl + "thumb/";
                    foreach (ActivityProviderData sd in res.ResultData)
                    {
                        if (sd.profile_picture == "" || sd.profile_picture == null)
                        {
                            sd.profile_picture = "";
                        }
                        else
                        {
                            sd.profile_picture = imgurl + sd.profile_picture;
                        }
                    }


                    foreach (ProviderData sd in resAct.ResultData)
                    {
                        if (sd.providerpicture == "" || sd.providerpicture == null)
                        {
                            sd.providerpicture = "";
                        }
                        else
                        {
                            sd.providerpicture = thumurl + sd.providerpicture;
                        }

                        if (sd.profilepicture == "" || sd.profilepicture == null)
                        {
                            sd.profilepicture = "";
                        }
                        else
                        {
                            sd.profilepicture = thumurl + sd.profilepicture;
                        }
                    }

                    foreach (ActivityProviderData sd in resProf.ResultData)
                    {
                        if (sd.profile_picture == "" || sd.profile_picture == null)
                        {
                            sd.profile_picture = "";
                        }
                        else
                        {
                            sd.profile_picture = thumurl + sd.profile_picture;
                        }
                    }


                    return new
                    {
                        result = 1,
                        data = res.ResultData.Select(x => new
                        {
                            provider_id = x.activity_provider_id,
                            x.first_name,
                            x.last_name,
                            x.email,
                            x.location,
                            provider_profile_picture = x.profile_picture,
                            x.star_rating,
                            x.about_me,
                            provider_slug_url = x.slug_url,
                            x.profession
                        }).FirstOrDefault(),
                        dataActivity = resAct.ResultData.Select(x => new
                        {
                            activity_id = x.activity_master_id,
                            provider_id = x.activity_provider_id,
                            x.first_name,
                            x.last_name,
                            x.location,
                            activity_title = x.title,
                            x.description,
                            activity_price = x.price,
                            activity_picture = x.providerpicture,
                            provider_profile_picture = x.profilepicture,
                            x.highlight,
                            x.star_rating,
                            x.short_description,
                            x.tags_name,
                            x.slug_url,
                            x.city_slug_url,
                            x.provider_slug_url,
                            x.profession
                        }),
                        dataProfile = resProf.ResultData.Select(x => new
                        {
                            provider_id = x.activity_provider_id,
                            x.first_name,
                            x.last_name,
                            x.email,
                            x.location,
                            provider_profile_picture = x.profile_picture,
                            x.star_rating,
                            x.about_me,
                            provider_slug_url = x.slug_url,
                            x.profession
                        })
                    } as dynamic;
                }
                else
                {
                    return new { result = 0, data = "" } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new { result = 0, data = "" } as dynamic;
            }
        }

        [HttpPost]
        //public dynamic signupFormData(signupForm data)
        public dynamic signupFormData(EncryptPostData encryptdata)
        {
            try
            {
                string decryptdata = AESEncryption.DecryptStringAES(encryptdata.hdndata);
                signupForm data = JsonConvert.DeserializeObject<signupForm>(decryptdata);
                ResultObject<string> res = new ResultObject<string>();
                SignupRequestData sign = new SignupRequestData();
                sign.name = data.fullName;
                sign.email = data.email;
                sign.message = data.message;
                sign.subject = data.subject;
                res = _IAdminMgr.editSignMaster("updatesignupr", "", sign);
                if (res.Result == ResultType.Success)
                {
                    ResultObject<List<ConfigurationData>> resConfig = _IAdminMgr.viewConfiguration("viewconfiguation", "", ""); ;
                    string body = "Hello " + data.fullName! + "  <br/><br/>" +
                                "Thank you for reaching us out. One of our representatives will surely contact you shortly. <br/><br/>" +
                                "Looking forward to talk to you soon! <br/>" +
                                "Team Nearby Experiences.<br/>";

                    string subject = "We will get back soon!";

                    string startupPath = System.IO.Directory.GetCurrentDirectory();
                    string FilePathAdmin = startupPath + "\\user.html";
                    StreamReader strAdmin = new StreamReader(FilePathAdmin);
                    string MailTextAdmin = strAdmin.ReadToEnd();
                    strAdmin.Close();
                    MailTextAdmin = MailTextAdmin.Replace("href=facebook", "href=" + resConfig.ResultData.Select(x => x.config_facebook).FirstOrDefault() + "");
                    MailTextAdmin = MailTextAdmin.Replace("href=insta", "href=" + resConfig.ResultData.Select(x => x.config_instagram).FirstOrDefault() + "");
                    MailTextAdmin = MailTextAdmin.Replace("href=twitter", "href=" + resConfig.ResultData.Select(x => x.config_twitter).FirstOrDefault() + "");
                    MailTextAdmin = MailTextAdmin.Replace("typesettingindustry@gmail.com", resConfig.ResultData.Select(x => x.config_email).FirstOrDefault() + "");
                    body = MailTextAdmin;
                    string check = SentEmail.emailSentUat(body, subject, SentEmail.fromemailtest, data.email);
                    if (check == "1")
                    {
                        subject = "New enquiry received.";
                        body = "Hello Admin <br/><br/>" +
                            "New enquiry received.<br/>" +
                             "You have received an enquiry from the following user:-<br/>" +
                             "Name:  " + data.fullName + "  <br/>" +
                             "Email:  " + data.email + "  <br/>" +
                             "Subject: " + data.message + "  <br/>" +
                             "Message:  " + data.subject + "  <br/>" +
                        "Team Nearby Experiences. <br/>";
                        string FilePath = startupPath + "\\admin.html";
                        StreamReader str = new StreamReader(FilePath);
                        string MailText = str.ReadToEnd();
                        str.Close();
                        MailText = MailText.Replace("Mahi Kumawat", data.fullName.Trim());
                        MailText = MailText.Replace("MahiKumawat@gmail.com", data.email.Trim());
                        MailText = MailText.Replace("For mail inquiry form contact page", data.subject.Trim());
                        MailText = MailText.Replace("Lorem Ipsum is", data.message.Trim());
                        MailText = MailText.Replace("href=facebook", "href=" + resConfig.ResultData.Select(x => x.config_facebook).FirstOrDefault() + "");
                        MailText = MailText.Replace("href=insta", "href=" + resConfig.ResultData.Select(x => x.config_instagram).FirstOrDefault() + "");
                        MailText = MailText.Replace("href=twitter", "href=" + resConfig.ResultData.Select(x => x.config_twitter).FirstOrDefault() + "");
                        body = MailText;
                        check = SentEmail.emailSentUat(body, subject, SentEmail.fromemailtest, SentEmail.fromemailtest);
                    }
                    return new
                    {
                        result = 1,
                        data = res.ResultData
                    } as dynamic;
                }
                else
                {
                    return new { result = 0, data = "" } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new { result = 0, data = "" } as dynamic;
            }
        }

        [HttpPost]
        public dynamic getActivityMetaDetails(EncryptPostData encryptdata)
        {
            try
            {
                string decryptdata = AESEncryption.DecryptStringAES(encryptdata.hdndata);
                activity data = JsonConvert.DeserializeObject<activity>(decryptdata);
                ResultObject<List<ProviderData>> res = _IProviderMgr.getProvider("viewActivityDetails", data.slugKey, "");
                if (res.Result == ResultType.Success)
                {
                    string imgurl = $"{_httpContextAccessor.HttpContext.Request.Scheme}://{_httpContextAccessor.HttpContext.Request.Host}";
                    imgurl = imgurl + "/images/";


                    foreach (ProviderData sd in res.ResultData)
                    {
                        if (sd.providerpicture == "" || sd.providerpicture == null)
                        {
                            sd.providerpicture = "";
                        }
                        else
                        {
                            sd.providerpicture = imgurl + sd.providerpicture;
                        }
                    }
                    return new
                    {
                        result = 1,
                        data = res.ResultData.Select(x => new
                        {
                            meta_title = x.metatitle,
                            meta_description = x.metadescription,
                            meta_keywords = x.metakeywords,
                            activity_picture = x.providerpicture,
                        }).FirstOrDefault()
                    } as dynamic;
                }
                else
                {
                    return new { result = 0, data = "" } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new { result = 0, data = "" } as dynamic;
            }
        }

        [HttpPost]
        public dynamic getProviderMetaDetails(EncryptPostData encryptdata)
        {
            try
            {
                string decryptdata = AESEncryption.DecryptStringAES(encryptdata.hdndata);
                activity data = JsonConvert.DeserializeObject<activity>(decryptdata);
                ResultObject<List<ActivityProviderData>> res = _IAdminMgr.acitvityProvider("viewactivityProviderbyidApi", data.slugKey, "");
                if (res.Result == ResultType.Success)
                {
                    string imgurl = $"{_httpContextAccessor.HttpContext.Request.Scheme}://{_httpContextAccessor.HttpContext.Request.Host}";
                    imgurl = imgurl + "/images/";
                    foreach (ActivityProviderData sd in res.ResultData)
                    {
                        if (sd.profile_picture == "" || sd.profile_picture == null)
                        {
                            sd.profile_picture = "";
                        }
                        else
                        {
                            sd.profile_picture = imgurl + sd.profile_picture;
                        }
                    }
                    return new
                    {
                        result = 1,
                        data = res.ResultData.Select(x => new
                        {
                            meta_title = x.metatitle,
                            meta_description = x.metadescription,
                            meta_keywords = x.metakeywords,
                            provider_profile_picture = x.profile_picture

                        }).FirstOrDefault(),
                    } as dynamic;
                }
                else
                {
                    return new { result = 0, data = "" } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new { result = 0, data = "" } as dynamic;
            }
        }

        [HttpPost]
        //public dynamic getCmsMeta(activity data)
        public dynamic getCmsMeta(EncryptPostData encryptdata)
        {
            try
            {
                string decryptdata = AESEncryption.DecryptStringAES(encryptdata.hdndata);
                activity data = JsonConvert.DeserializeObject<activity>(decryptdata);
                ResultObject<List<CmsPageData>> res = new ResultObject<List<CmsPageData>>();
                if (data.term == "terms")
                {
                    res = _IAdminMgr.cmsMaster("viewcmsreq", "Terms & Condition", "");
                }
                else if (data.term == "privacy")
                {
                    res = _IAdminMgr.cmsMaster("viewcmsreq", "Privacy Policy", "");
                }

                if (res.Result == ResultType.Success)
                {
                    return new
                    {
                        result = 1,
                        data = res.ResultData.Select(x => new
                        {
                            meta_title = x.page_metatitle,
                            meta_description = x.page_metadescription,
                            meta_keywords = x.page_metakeywords
                        }).FirstOrDefault()
                    } as dynamic;
                }
                else
                {
                    return new { result = 0, data = "" } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new { result = 0, data = "" } as dynamic;
            }
        }

        [HttpPost]
        //public dynamic getActivityWidget(activity data)
        public dynamic getActivityWidget(EncryptPostData encryptdata)
        {
            try
            {
                string decryptdata = AESEncryption.DecryptStringAES(encryptdata.hdndata);
                activity data = JsonConvert.DeserializeObject<activity>(decryptdata);
                ResultObject<List<ProviderData>> res = _IProviderMgr.getProvider("viewActivityDetails", data.slugKey, "");
                if (res.Result == ResultType.Success)
                {
                    return new
                    {
                        result = 1,
                        data = res.ResultData.Select(x => new
                        {
                            x.widget_code
                        }).FirstOrDefault()
                    } as dynamic;
                }
                else
                {
                    return new { result = 0, data = "" } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new { result = 0, data = "" } as dynamic;
            }
        }


        [HttpPost]
        //public dynamic getHomeActivity(expolorActivity data)
        public dynamic getHomeActivity(EncryptPostData encryptdata)
        {
            try
            {
                string decryptdata = AESEncryption.DecryptStringAES(encryptdata.hdndata);
                expolorActivity data = JsonConvert.DeserializeObject<expolorActivity>(decryptdata);
                ResultObject<List<ProviderData>> recent = _IProviderMgr.getProviderCity("viewactivityMasterAPI", "recentadd", "", data.city);
                ResultObject<List<ProviderData>> feature = _IProviderMgr.getProviderCity("viewactivityMasterAPI", "feature", "", data.city);
                ResultObject<List<ProviderData>> popular = _IProviderMgr.getProviderCity("viewactivityMasterAPI", "popular", "", data.city);
                if (recent.Result == ResultType.Success)
                {
                    string imgurl = $"{_httpContextAccessor.HttpContext.Request.Scheme}://{_httpContextAccessor.HttpContext.Request.Host}";
                    imgurl = imgurl + "/images/thumb/";

                    foreach (ProviderData sd in recent.ResultData)
                    {
                        if (sd.providerpicture == "" || sd.providerpicture == null)
                        {
                            sd.providerpicture = "";
                        }
                        else
                        {
                            sd.providerpicture = imgurl + sd.providerpicture;
                        }

                        if (sd.profilepicture == "" || sd.profilepicture == null)
                        {
                            sd.profilepicture = "";
                        }
                        else
                        {
                            sd.profilepicture = imgurl + sd.profilepicture;
                        }
                    }

                    foreach (ProviderData sd in feature.ResultData)
                    {
                        if (sd.providerpicture == "" || sd.providerpicture == null)
                        {
                            sd.providerpicture = "";
                        }
                        else
                        {
                            sd.providerpicture = imgurl + sd.providerpicture;
                        }

                        if (sd.profilepicture == "" || sd.profilepicture == null)
                        {
                            sd.profilepicture = "";
                        }
                        else
                        {
                            sd.profilepicture = imgurl + sd.profilepicture;
                        }
                    }

                    foreach (ProviderData sd in popular.ResultData)
                    {
                        if (sd.providerpicture == "" || sd.providerpicture == null)
                        {
                            sd.providerpicture = "";
                        }
                        else
                        {
                            sd.providerpicture = imgurl + sd.providerpicture;
                        }

                        if (sd.profilepicture == "" || sd.profilepicture == null)
                        {
                            sd.profilepicture = "";
                        }
                        else
                        {
                            sd.profilepicture = imgurl + sd.profilepicture;
                        }
                    }

                    return new
                    {
                        result = 1,
                        data = new
                        {
                            recent = recent.ResultData.Select(x => new
                            {
                                activity_id = x.activity_master_id,
                                provider_id = x.activity_provider_id,
                                x.first_name,
                                x.last_name,
                                x.location,
                                activity_title = x.title,
                                x.short_description,
                                activity_price = x.price,
                                activity_picture = x.providerpicture,
                                provider_profile_picture = x.profilepicture,
                                x.slug_url,
                                x.provider_slug_url,
                                x.profession

                            }),
                            feature = feature.ResultData.Select(x => new
                            {
                                activity_id = x.activity_master_id,
                                provider_id = x.activity_provider_id,
                                x.first_name,
                                x.last_name,
                                x.location,
                                activity_title = x.title,
                                x.short_description,
                                activity_price = x.price,
                                activity_picture = x.providerpicture,
                                provider_profile_picture = x.profilepicture,
                                x.slug_url,
                                x.provider_slug_url,
                                x.profession
                            }),
                            popular = popular.ResultData.Select(x => new
                            {
                                activity_id = x.activity_master_id,
                                provider_id = x.activity_provider_id,
                                x.first_name,
                                x.last_name,
                                x.location,
                                activity_title = x.title,
                                x.short_description,
                                activity_price = x.price,
                                activity_picture = x.providerpicture,
                                provider_profile_picture = x.profilepicture,
                                x.slug_url,
                                x.provider_slug_url,
                                x.profession
                            })
                        }
                    };
                }
                else
                {
                    return new { result = 0, data = "" } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new { result = 0, data = "" } as dynamic;
            }
        }

        [HttpPost]
        public dynamic updateFaqSort(string itemIDs)
        {
            //int count = 1;
            //List<int> itemidList = new List<int>();
            //itemidList = itemIDs.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToList();
            FaqData _testm = new FaqData();
            ResultObject<string> res = new ResultObject<string>();
            try
            {
                res = _IAdminMgr.editfaq("sortingfaq", itemIDs, _testm);
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            //return res;
            return new
            {
                data = "success"
            } as dynamic;
        }

    }
}