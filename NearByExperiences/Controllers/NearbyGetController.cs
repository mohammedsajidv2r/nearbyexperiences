﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using NE_BaseObject;
using NE_BaseObject.Admin;
using NE_BaseObject.Api;
using NE_Mgr_Interface.Admin;
using NE_Mgr_Interface.Provider;
using NE_Utilities.Enum;

namespace NearByExperiences.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class NearbyGetController : ControllerBase
    {
        IConfiguration _iConfig;
        IHttpContextAccessor _httpContextAccessor;
        IAdminMgr _IAdminMgr;
        IProviderMgr _IProviderMgr;

        public NearbyGetController(IAdminMgr IAdminMgr, IConfiguration iConfig, IHttpContextAccessor httpContextAccessor, IProviderMgr IProviderMgr)
        {
            _iConfig = iConfig;
            _httpContextAccessor = httpContextAccessor;
            _IAdminMgr = IAdminMgr;
            _IProviderMgr = IProviderMgr;
        }

        [HttpGet]
        public dynamic getCity()
        {
            try
            {
                ResultObject<List<CityMasterData>> res = _IAdminMgr.cityMaster("viewcityApi", "", "");
                if (res.Result == ResultType.Success)
                {
                    return new { result = 1, data = res.ResultData.Select(x => new { x.city_id, x.city_name, x.slug_url }) } as dynamic;
                }
                else
                {
                    return new { result = 0, data = "" } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new { result = 0, data = "" } as dynamic;
            }
        }

        [HttpGet]
        public dynamic getProviderProfile()
        {
            try
            {
                ResultObject<List<ActivityProviderData>> res = _IAdminMgr.acitvityProvider("viewactivityProviderApi", "", "");
                if (res.Result == ResultType.Success)
                {
                    string imgurl = $"{_httpContextAccessor.HttpContext.Request.Scheme}://{_httpContextAccessor.HttpContext.Request.Host}";
                    imgurl = imgurl + "/images/thumb/";

                    foreach (ActivityProviderData sd in res.ResultData)
                    {
                        if (sd.profile_picture == "" || sd.profile_picture == null)
                        {
                            sd.profile_picture = "";
                        }
                        else
                        {
                            sd.profile_picture = imgurl + sd.profile_picture;
                        }
                    }
                    return new
                    {
                        result = 1,
                        data = res.ResultData.Select(x => new
                        {
                            provider_id = x.activity_provider_id,
                            x.first_name,
                            x.last_name,
                            x.about_me,
                            x.profile_picture,
                            //profile_picture = imgurl + x.profile_picture,
                            x.star_rating,
                            provider_slug_url = x.slug_url,
                            x.profession
                        })
                    } as dynamic;
                }
                else
                {
                    return new { result = 0, data = "" } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new { result = 0, data = "" } as dynamic;
            }
        }

        [HttpGet]
        public dynamic getTestimonials()
        {
            try
            {
                ResultObject<List<TestimonialData>> res = _IAdminMgr.tetimonial("viewTestmonialApi", "", "");
                if (res.Result == ResultType.Success)
                {
                    string imgurl = $"{_httpContextAccessor.HttpContext.Request.Scheme}://{_httpContextAccessor.HttpContext.Request.Host}";
                    imgurl = imgurl + "/images/thumb/";
                    foreach (TestimonialData sd in res.ResultData)
                    {
                        if (sd.testimonial_image == "" || sd.testimonial_image == null)
                        {
                            sd.testimonial_image = "";
                        }
                        else
                        {
                            sd.testimonial_image = imgurl + sd.testimonial_image;
                        }
                    }
                    return new
                    {
                        result = 1,
                        data = res.ResultData.Select(x => new
                        {
                            x.testimonial_id,
                            name = x.testimonial_name,
                            description = x.testimonial_description,
                            profile_picture = x.testimonial_image,
                            x.designation
                        })
                    } as dynamic;
                }
                else
                {
                    return new { result = 0, data = "" } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new { result = 0, data = "" } as dynamic;
            }
        }

        [HttpGet]
        public dynamic getPrivacy()
        {
            try
            {
                ResultObject<List<CmsPageData>> res = _IAdminMgr.cmsMaster("viewcmsreq", "Privacy Policy", ""); ;
                if (res.Result == ResultType.Success)
                {
                    return new { result = 1, data = res.ResultData.Select(x => new { x.cms_id, title = x.page_title, content = x.page_content }).FirstOrDefault() } as dynamic;
                }
                else
                {
                    return new { result = 0, data = "" } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new { result = 0, data = "" } as dynamic;
            }
        }

        [HttpGet]
        public dynamic getTerms()
        {
            try
            {
                ResultObject<List<CmsPageData>> res = _IAdminMgr.cmsMaster("viewcmsreq", "Terms & Condition", ""); ;
                if (res.Result == ResultType.Success)
                {
                    return new { result = 1, data = res.ResultData.Select(x => new { x.cms_id, title = x.page_title, content = x.page_content }).FirstOrDefault() } as dynamic;
                }
                else
                {
                    return new { result = 0, data = "" } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new { result = 0, data = "" } as dynamic;
            }
        }

        [HttpGet]
        public dynamic getConfiguration()
        {
            try
            {
                ResultObject<List<ConfigurationData>> res = _IAdminMgr.viewConfiguration("viewconfiguation", "", ""); ;
                if (res.Result == ResultType.Success)
                {
                    return new
                    {
                        result = 1,
                        data = res.ResultData.Select(x => new
                        {
                            x.config_id,
                            x.config_facebook,
                            x.config_instagram,
                            x.config_linkedin,
                            x.config_twitter,
                            x.config_email,
                            x.config_contact,
                            x.config_website,
                            x.config_location
                        }).FirstOrDefault()
                    } as dynamic;
                }
                else
                {
                    return new { result = 0, data = "" } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new { result = 0, data = "" } as dynamic;
            }
        }

        [HttpGet]
        public dynamic getFAQ()
        {
            try
            {
                ResultObject<List<FaqData>> res = _IAdminMgr.viewfaq("viewFaqApi", "", ""); ;
                if (res.Result == ResultType.Success)
                {
                    return new
                    {
                        result = 1,
                        data = res.ResultData.Select(x => new
                        {
                            x.faq_id,
                            x.faq_question,
                            x.faq_answer
                        })
                    } as dynamic;
                }
                else
                {
                    return new { result = 0, data = "" } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new { result = 0, data = "" } as dynamic;
            }
        }


        [HttpGet]
        public dynamic getFAQMetaDetails()
        {
            try
            {
                ResultObject<List<CmsPageData>> res = _IAdminMgr.cmsMaster("viewcmsreq", "Page", "");
                if (res.Result == ResultType.Success)
                {
                    return new
                    {
                        result = 1,
                        data = res.ResultData.Select(x => new
                        {
                            x.cms_id,
                            title = x.page_title,
                            meta_title = x.page_metatitle,
                            meta_description = x.page_metadescription,
                            meta_keywords = x.page_metakeywords
                        }).FirstOrDefault()
                    } as dynamic;
                }
                else
                {
                    return new { result = 0, data = "" } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new { result = 0, data = "" } as dynamic;
            }
        }

        [HttpGet]
        public dynamic getAllProviders()
        {
            try
            {
                ResultObject<List<ActivityProviderData>> res = _IAdminMgr.acitvityProvider("viewactivityProviderApiAll", "", "");
                if (res.Result == ResultType.Success)
                {
                    string imgurl = $"{_httpContextAccessor.HttpContext.Request.Scheme}://{_httpContextAccessor.HttpContext.Request.Host}";
                    imgurl = imgurl + "/images/thumb/";

                    foreach (ActivityProviderData sd in res.ResultData)
                    {
                        if(sd.profile_picture =="" || sd.profile_picture == null)
                        {
                            sd.profile_picture = "";
                        }
                        else
                        {
                            sd.profile_picture = imgurl + sd.profile_picture;
                        }
                    }
                    return new
                    {
                        result = 1,
                        data = res.ResultData.Select(x => new
                        {
                            provider_id = x.activity_provider_id,
                            x.first_name,
                            x.last_name,
                            x.about_me,
                            x.profile_picture ,
                            x.star_rating,
                            provider_slug_url = x.slug_url,
                            x.profession
                        })
                    } as dynamic;
                }
                else
                {
                    return new { result = 0, data = "" } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new { result = 0, data = "" } as dynamic;
            }
        }

    }
}