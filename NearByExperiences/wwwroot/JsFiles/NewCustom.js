﻿function CSLoaderFadeIn() {
    $('.theme-loader').fadeIn('slow', function () {
    });
}

function CSLoaderFadeOut() {
    $('.theme-loader').fadeOut('slow', function () {
    });
}


function CSShowModel(ModelID) {
    $("#" + ModelID).modal('show');
}

function CSHideModel(ModelID) {
    $("#" + ModelID).modal('hide');
}

function CSAlertMsg(type, msg) {
    Snackbar.show({
        text: msg,
        pos: 'top-center'
    });
}

function CSReloadFunc() {
    location.reload();
}


function CSBindDataTableRemove(tableId) {
    $('#' + tableId).DataTable().destroy();
}

function CSBindingMultiSelectComp1(id, val) {
    $("#" + id).selectpicker({
        noneSelectedText: 'Select',
        noneResultsText: 'No Data matched {0}',
    });
    $("#" + id).selectpicker('val', val);
}


function CSBindTermsCkeditor() {
    CKEDITOR.config.removeButtons =
        'Save,NewPage,Preview,Print,PasteFromWord,PasteText,Paste,Copy,Cut,Undo,Redo,Replace,Find,SelectAll,Scayt,Form,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Subscript,Superscript,Templates,Checkbox,CopyFormatting,RemoveFormat,NumberedList,BulletedList,Outdent,Indent,CreateDiv,BidiLtr,BidiRtl,Language,Anchor,Image,Flash,HorizontalRule,Smiley,SpecialChar,PageBreak,Iframe,Maximize,ShowBlocks,About,Styles';
    CKEDITOR.replace('shortdescription');
    CKEDITOR.replace('fulldescription');
    CKEDITOR.add
}



function CSBindCmsCkeditor() {
   // if (CKEDITOR.instances['contentval']) CKEDITOR.instances['contentval'].destroy();
    CKEDITOR.config.removeButtons =
        'Save,NewPage,Preview,Print,PasteFromWord,PasteText,Paste,Copy,Cut,Undo,Redo,Replace,Find,SelectAll,Scayt,Form,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Subscript,Superscript,Templates,Checkbox,CopyFormatting,RemoveFormat,NumberedList,BulletedList,Outdent,Indent,CreateDiv,BidiLtr,BidiRtl,Language,Anchor,Image,Flash,HorizontalRule,Smiley,SpecialChar,PageBreak,Iframe,Maximize,ShowBlocks,About,Styles';
    CKEDITOR.replace('contentval');
    CKEDITOR.add
}

function getShortDescEditor() {
    var desc = CKEDITOR.instances['shortdescription'].getData();
    return desc
}
function getDescEditor() {
    var desc = CKEDITOR.instances['fulldescription'].getData();
    return desc
}
function getEditor(id) {
    var desc = CKEDITOR.instances[id].getData();
    return desc
}

function CSWindowReload() {
    window.location.reload();
}

window.setTitle = (title) => {
   // console.log('Setting title', title);
    document.title = title;
}


function CSBindDashboardNear(USubData, provsplit, actsplit) {
    UsubNew = [];
    UsubRenewals = [];
    UsubDate = [];
    Usubtotal = [];
    UsubNew.push(USubData.cityCount);
    UsubRenewals.push(USubData.tagCount);
    UsubDate.push(USubData.providerCount);
    Usubtotal.push(USubData.activityCount);
    try {

        // Registered App Users
        //var regUser = [30, 60, 38, 52, 36, 40, 28, 38, 60, 38, 52, 36, 40, 28, 38];
        var regAppUser = {
            chart: {
                id: '',
                type: 'area',
                height: 50,
                sparkline: {
                    enabled: true
                },
            },
            stroke: {
                curve: 'smooth',
                width: 2,
            },
            series: [{
                name: 'Provider',
                data: provsplit
            }],
            labels: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15'],
            yaxis: {
                min: 0,
                max: Math.max(...provsplit) + 5
            },
            colors: ['#1b55e2'],
            tooltip: {
                x: {
                    show: false,
                }
            },
            fill: {
                type: "gradient",
                gradient: {
                    type: "vertical",
                    shadeIntensity: 1,
                    inverseColors: !1,
                    opacityFrom: .40,
                    opacityTo: .05,
                    stops: [45, 100]
                }
            },
        }

        // Unregistered App Users
        //var unRegUser = [10, 20, 20, 25, 10, 15, 10, 20, 25, 22, 25, 20, 25, 15, 10];
        var unregAppUser = {
            chart: {
                id: '',
                type: 'area',
                height: 50,
                margin: 10,
                sparkline: {
                    enabled: true
                },
            },
            stroke: {
                curve: 'smooth',
                width: 2,
            },
            series: [{
                name: 'Activity',
                data: actsplit
            }],
            labels: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15'],
            yaxis: {
                min: 0,
                max: Math.max(...actsplit) + 5
            },
            colors: ['#e7515a'],
            tooltip: {
                x: {
                    show: false,
                }
            },
            fill: {
                type: "gradient",
                gradient: {
                    type: "vertical",
                    shadeIntensity: 1,
                    inverseColors: !1,
                    opacityFrom: .40,
                    opacityTo: .05,
                    stops: [45, 100]
                }
            },
        }

        // User Subscriptions
        var userSubscribtions = {
            chart: {
                height: 350,
                type: 'bar',
                stacked: true,
                toolbar: {
                    show: false,
                }
            },
            responsive: [{
                breakpoint: 480,
                options: {
                    legend: {
                        position: 'top',
                        offsetX: -10,
                        offsetY: 0
                    }
                }
            }],
            plotOptions: {
                bar: {
                    horizontal: false,
                },
            },
            series: [{
                name: 'Renewals',
                data: UsubRenewals
            }, {
                name: 'New Subscribers',
                data: UsubNew
            },
            {
                name: 'Total Subscriptions',
                data: Usubtotal
            },
            ],
            xaxis: {
                type: 'text',
                categories: UsubDate,
            },
            legend: {
                position: 'top',
            },
            fill: {
                opacity: 1
            },
        }
        /*
            ==============================
                Statistics | Script
            ==============================
        */


        // Registered App Users
        var d_1C_5 = new ApexCharts(document.querySelector("#regAppChart"), regAppUser);
        d_1C_5.render()

        // Unregistered App Users

        var d_1C_6 = new ApexCharts(document.querySelector("#unregAppChart"), unregAppUser);
        d_1C_6.render()

        // Total Web Order
        var d_1C_7 = new ApexCharts(document.querySelector("#totWebChart"), regAppUser);
        d_1C_7.render()

        // Total Order Users
        var d_1C_8 = new ApexCharts(document.querySelector("#totOrderUser"), unregAppUser);
        d_1C_8.render()

        // User Subscriptions
        var chart = new ApexCharts(document.querySelector("#userSubsChart"), userSubscribtions);
        chart.render();
    }
    catch (e) {
        console.log(e);
    }

}
arrSum = function (ar) {
    return ar.reduce(function (a, b) {
        return a + b
    }, 0);
}


function CSBindDataTable(tableId, customBind, DeforderColNo, sorting) {
    DtDynamicVal = $('#' + tableId).DataTable({
        "order": [DeforderColNo, "asc"],
        "processing": "true",

        'aoColumnDefs': [{
            'bSortable': false,
            'aTargets': [-1] /* 1st one, start by the right */
        }],
        //columnDefs: [{ type: 'date', 'targets': [DeforderColNo] }],
        "language": {
            "processing": "processing... please wait",
            paginate: {
                next: '&#8594;', // or '→'
                previous: '&#8592;' // or '←'
            }
        },
        "oLanguage": {
            "oPaginate": {
                "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
                "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'
            },
            "sInfo": "Showing page _PAGE_ of _PAGES_",
            "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
            "sSearchPlaceholder": "Search...",
            "sLengthMenu": "Show :  _MENU_ enteries.",
        },
        "stripeClasses": [],

    });
    $.fn.dataTable.ext.errMode = 'none';
}

function BindProviderdtexcel(tableId, customBind, DeforderColNo, sorting) {
    $.fn.dataTable.ext.errMode = 'none';
    $('#' + tableId).DataTable({
        dom: '<"row"<"col-md-12"<"row"<"col-xl-2 col-lg-3 col-md-3 col-sm-12 col-12"l><"col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6"B><"col-xl-6 col-md-5 col-lg-5 col-sm-6 col-6 p-0"f> > ><"col-md-12 "rt> <"col-md-12"<"row"<"col-md-5 p-0"i><"col-md-7"p>>> >',
        buttons: {
            buttons: [{
                extend: 'csv',
                className: 'btn',
                exportOptions: {
                    columns: [2, 3, 5]
                }
            }, {
                extend: 'excel',
                className: 'btn',
                exportOptions: {
                    columns: [2, 3, 5]
                }
            }]
        },
        "order": [DeforderColNo, "asc"],
        "aoColumns": [
            { "bSortable": false },
            { "bSortable": false },
            null,
            null,
            { "bSortable": false }, // <-- disable sorting for column 3
            null,
            null
        ],
        'aoColumnDefs': [{
            'bSortable': false,
            'aTargets': [-1] /* 1st one, start by the right */
        }],
        "language": {
            "processing": "processing... please wait",
            paginate: {
                next: '&#8594;', // or '→'
                previous: '&#8592;' // or '←'
            }
        },
        "oLanguage": {
            "oPaginate": {
                "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
                "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'
            },
            "sInfo": "Showing page _PAGE_ of _PAGES_",
            "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
            "sSearchPlaceholder": "Search...",
            "sLengthMenu": "Show :  _MENU_ ",
        },
        "stripeClasses": [],
    });
}




function BindActivitydtexcel(tableId, customBind, DeforderColNo, sorting) {
    $.fn.dataTable.ext.errMode = 'none';

    var table = $('#' + tableId).DataTable({
        dom: '<"row"<"col-md-12"<"row"<"col-xl-2 col-lg-3 col-md-3 col-sm-12 col-12"l><"col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6"B><"col-xl-6 col-md-5 col-lg-5 col-sm-6 col-6 p-0"f> > ><"col-md-12 "rt> <"col-md-12"<"row"<"col-md-5 p-0"i><"col-md-7"p>>> >',
        buttons: {
            buttons: [{
                extend: 'csv',
                className: 'btn',
                exportOptions: {
                    columns: [2, 3, 4, 5, 6, 7]
                }
            },
            {
                extend: 'excel',
                className: 'btn',
                exportOptions: {
                    columns: [2, 3, 4, 5, 6, 7]
                }
            }]
        },
        "aoColumns": [
            { "bSortable": false },
            { "bSortable": false },
            null,
            null,
            null,// <-- disable sorting for column 3
            null,
            null,
            null,
            null
        ],
        "order": [DeforderColNo, "asc"],
        'aoColumnDefs': [{
            'bSortable': false,
            'aTargets': [-1] /* 1st one, start by the right */
        }],
        "language": {
            "processing": "processing... please wait",
            paginate: {
                next: '&#8594;', // or '→'
                previous: '&#8592;' // or '←'
            }
        },
        "oLanguage": {
            "oPaginate": {
                "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
                "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'
            },
            "sInfo": "Showing page _PAGE_ of _PAGES_",
            "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
            "sSearchPlaceholder": "Search...",
            "sLengthMenu": "Show :  _MENU_ ",
        },
        "stripeClasses": [],
    });
}


function CSBindDataTableCity(tableId, customBind, DeforderColNo, sorting) {
    DtDynamicVal = $('#' + tableId).DataTable({

        "order": [DeforderColNo, "asc"],
        "columnDefs": [{
            "searchable": false,
            "orderable": false,
            "targets": 0
        }],
        "language": {
            "processing": "processing... please wait",
            paginate: {
                next: '&#8594;', // or '→'
                previous: '&#8592;' // or '←'
            }
        },
        "oLanguage": {
            "oPaginate": {
                "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
                "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'
            },
            "sInfo": "Showing page _PAGE_ of _PAGES_",
            "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
            "sSearchPlaceholder": "Search...",
            "sLengthMenu": "Show :  _MENU_ enteries.",
        },
        "stripeClasses": [],

    });
    DtDynamicVal.on('order.dt search.dt', function () {
        DtDynamicVal.column(0, { search: 'applied', order: 'applied', bSortable: false }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
    $.fn.dataTable.ext.errMode = 'none';
}



function CSBindDataTableCityWithCheck(tableId, customBind, DeforderColNo, sorting) {

    $.fn.dataTable.ext.errMode = 'none';
    c2 = $('#' + tableId).DataTable({
        columnDefs: [{
            "searchable": false,
            "orderable": false,
            targets: 0,
        }],
        "aoColumns": [
            { "bSortable": false },
            { "bSortable": false },
            null,
            null,
            null
        ],
        'aoColumnDefs': [{
            'bSortable': false,
            'aTargets': [-1] /* 1st one, start by the right */
        }],
        "order": [DeforderColNo, "asc"],
        "language": {
            "processing": "processing... please wait",
            paginate: {
                next: '&#8594;', // or '→'
                previous: '&#8592;' // or '←'
            }
        },
        "oLanguage": {
            "oPaginate": {
                "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
                "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'
            },
            "sInfo": "Showing page _PAGE_ of _PAGES_",
            "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
            "sSearchPlaceholder": "Search...",
            "sLengthMenu": "Show :  _MENU_ enteries.",
        },
    });

    c2.on('order.dt search.dt', function () {
        c2.column(1, { search: 'applied', order: 'applied', bSortable: false }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

    multiCheck(c2);
}


function CSBindDataTableTestimonial(tableId, customBind, DeforderColNo, sorting) {
    DtDynamicVal = $('#' + tableId).DataTable({
        "order": [DeforderColNo, "asc"],
        "processing": "true",

        "aoColumns": [
            { "bSortable": false },
            { "bSortable": false },
            null,
            null,
            null, // <-- disable sorting for column 3
            null
        ],

        'aoColumnDefs': [{
            'bSortable': false,
            'aTargets': [-1] /* 1st one, start by the right */
        }],
        //columnDefs: [{ type: 'date', 'targets': [DeforderColNo] }],
        "language": {
            "processing": "processing... please wait",
            paginate: {
                next: '&#8594;', // or '→'
                previous: '&#8592;' // or '←'
            }
        },
        "oLanguage": {
            "oPaginate": {
                "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
                "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'
            },
            "sInfo": "Showing page _PAGE_ of _PAGES_",
            "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
            "sSearchPlaceholder": "Search...",
            "sLengthMenu": "Show :  _MENU_ enteries.",
        },
        "stripeClasses": [],

    });
    $.fn.dataTable.ext.errMode = 'none';
}

function getcheckdtvalAll(tableId, val) {
    if (val == false) {
        $('input[type="checkbox"]', '#' + tableId).prop('checked', true);
    }
    else {
        $('input[type="checkbox"]', '#' + tableId).prop('checked', false);
    }
    $(this).toggleClass('allChecked');

}

function changespantext(id, value) {
    $('#' + id).html(value);
}

function imgupload() {
    $('#imgUpload').Jcrop({
        setSelect: [0, 0, 700, 700],
        aspectRatio: 1/1,
        minSize: [700, 700],
        //boxWidth: 300,   //Maximum width you want for your bigger images
        //boxHeight: 250,  //Maximum Height for your bigger image
        boxWidth: 600,   
        boxHeight: 400,  
        onSelect: SelectCropArea
    });
}

function SelectCropArea(c) {
    var ratioW = $('#imgUpload')[0].naturalWidth / $('#imgUpload').width();
    var ratioH = $('#imgUpload')[0].naturalHeight / $('#imgUpload').height();
    var currentRatio = Math.min(ratioW, ratioH);
    $('#valForX').val(parseInt(Math.round(c.x * currentRatio)));
    $('#valForY').val(parseInt(Math.round(c.y * currentRatio)));
    $('#valForW').val(parseInt(Math.round(c.w * currentRatio)));
    $('#valForH').val(parseInt(Math.round(c.h * currentRatio)));
}

function selectCropRange() {
    return $('#valForX').val() + "," + $('#valForY').val() + "," + $('#valForW').val() + "," + $('#valForH').val();
}

function destroypcropper() {
    $currentCropper = $('#imgUpload').data('cropper');
    $currentCropper.destroy();
}

function getcheckUncheckAll() {
    $("#checkall").prop("checked", false);
}


function cleardatatabel(tableId) {
    $('#' + tableId).DataTable().clear().draw()
}

function CSBindDataTableFaq(tableId, customBind, DeforderColNo, sorting) {
    DtDynamicVal1 = $('#' + tableId).DataTable({
       //'destroy': true,

        "processing": "true",
        "paging": false,
        "order": [1, "asc"],
        "aoColumns": [
            { "bSortable": false },
            { "bSortable": false },
            { "bSortable": false },
            { "bSortable": false },
            { "bSortable": false },
            { "bSortable": false },
        ],  
        "language": {
            "processing": "processing... please wait",
        },
        "oLanguage": {
            "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
            "sSearchPlaceholder": "Search...",
        },
    });
    $.fn.dataTable.ext.errMode = 'none';
    $('thead tr th').removeClass('sortfaq sorting_asc');
}


function faqsort() {
    $("#sortable").sortable({
        update: function (event, ui) {
            var itemIDs = "";
            $("#sortable").find(".tasksingleinline").each(function () {
                var itemid = $(this).attr("data-taskid");
                itemIDs = itemIDs + itemid + ",";
            });
            $.ajax({
                type: 'POST',
                datatype: "json",
                contentType: "application/json",
                url: 'api/NearbyPost/updateFaqSort?itemIDs=' + itemIDs,
                success: function (data) {
                    location.reload();
                }
            })
        }
    })
  
}


function CSBindFaqCkeditor() {
    $(window).on('load', function () {
        $('#ckeditor-textarea').ckeditor();
    });
    CKEDITOR.config.removeButtons =
        'Save,NewPage,Preview,Print,PasteFromWord,PasteText,Paste,Copy,Cut,Undo,Redo,Replace,Find,SelectAll,Scayt,Form,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Subscript,Superscript,Templates,Checkbox,CopyFormatting,RemoveFormat,NumberedList,BulletedList,Outdent,Indent,CreateDiv,BidiLtr,BidiRtl,Language,Anchor,Image,Flash,HorizontalRule,Smiley,SpecialChar,PageBreak,Iframe,Maximize,ShowBlocks,About,Styles';
    CKEDITOR.replace('contentval');
    CKEDITOR.add
}


function CSBindDataTableRemoveFaq(tableId) {
  $('#' + tableId).DataTable().destroy();
}