﻿using Microsoft.AspNetCore.Http;
using NE_BaseObject.Admin;
using NE_Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace NearByExperiences.Helpers
{
    public class SessionBootstrapper
    {
        private readonly IHttpContextAccessor accessor;
        private readonly SessionState session;
        public SessionBootstrapper(IHttpContextAccessor _accessor, SessionState _session)
        {
            accessor = _accessor;
            session = _session;
        }
        //public string SharedButtonLabel = "";

        public void Bootstrap()
        {
            //Singleton Item: services.AddSingleton<SessionState>(); in Startup.cs
            //Code to save data in server side session
            //If session already has data
            
            
            AdminUserData _authData = new AdminUserData();
            try
            {
                _authData = accessor.HttpContext.Session.GetObject<AdminUserData>("AuthLoginData");
            }
            catch(Exception ex)
            {

            }

         


            if (session.Items.ContainsKey("AuthLoginData") && _authData == null)
            {
                //get from singleton item
                _authData = session.Items["AuthLoginData"] as AdminUserData;
                // save to server side session
                accessor.HttpContext.Session.SetObject("AuthLoginData", _authData);
                //remove from singleton Item
                session.Items.Remove("AuthLoginData");
            }

            //If Session is not expired yet then  navigate to home
            if (accessor.HttpContext.Request.Path == "/logout")
            {
                accessor.HttpContext.Session.Clear();
                //accessor.HttpContext.Response.Redirect("/adminpanel/");
                accessor.HttpContext.Response.Redirect("/");
            }
            if (_authData != null && accessor.HttpContext.Request.Path == "/")
            {
                //accessor.HttpContext.Response.Redirect("/adminpanel/Dashboard");
                accessor.HttpContext.Response.Redirect("/dashboard");
            }
            //If Session is expired then navigate to login
            else if (_authData == null && accessor.HttpContext.Request.Path != "/")
            {
                if (accessor.HttpContext.Request.Path.ToString().Contains("/resetpassword"))
                {
                    //accessor.HttpContext.Session.Clear();
                    //accessor.HttpContext.Response.Redirect("/resetpassword");
                }
                else
                {
                    accessor.HttpContext.Response.Redirect("/");
                    //if (accessor.HttpContext.Request.Path.ToString().Contains("/#"))
                    //{
                    //    accessor.HttpContext.Response.Redirect("/dashboard");
                    //}
                    //else
                    //{
                    //}

                }
                // accessor.HttpContext.Response.Redirect("/adminpanel/");
                //accessor.HttpContext.Response.Redirect("/");
            }
        }

        public AdminUserData checkLoginSession()
        {
            AdminUserData _authData = new AdminUserData();
            _authData = accessor.HttpContext.Session.GetObject<AdminUserData>("AuthLoginData");
            return _authData;
        }
    }
}
