﻿using BlazorInputFile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NearByExperiences.FileUpload
{
    public interface IFileUploadData
    {
        Task UploadAsync(IFileListEntry file);
        Task UploadAsync(IFileListEntry file, string path);

        Task<string> UploadAsyncActivity(IFileListEntry file);

    }
}
