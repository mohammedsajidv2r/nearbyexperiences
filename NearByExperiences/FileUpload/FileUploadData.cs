﻿using BlazorInputFile;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Drawing;

namespace NearByExperiences.FileUpload
{
    public class FileUploadData : IFileUploadData
    {
        private readonly IWebHostEnvironment _environment;
        private readonly IHostingEnvironment _hostingEnvironment;

        public FileUploadData(IWebHostEnvironment env, IHostingEnvironment hostingEnvironment)
        {
            _environment = env;
            _hostingEnvironment = hostingEnvironment;
        }

        public async Task UploadAsync(IFileListEntry fileEntry)
        {
            var path = Path.Combine(_environment.ContentRootPath, "wwwroot\\images", fileEntry.Name);
            var ms = new MemoryStream();
            await fileEntry.Data.CopyToAsync(ms);
            using (FileStream file = new FileStream(path, FileMode.Create, FileAccess.Write))
            {
                ms.WriteTo(file);
                file.Dispose();
            }
            string thumpath = Path.Combine(_environment.ContentRootPath, "wwwroot\\images\\thumb", fileEntry.Name);
            System.Drawing.Image img1 = System.Drawing.Image.FromFile(path);
            System.Drawing.Image bmp1 = img1.GetThumbnailImage(263, 226, null, IntPtr.Zero);
            bmp1.Save(thumpath);
            ms.Dispose();
            img1.Dispose();
            bmp1.Dispose();
        }

        public  async Task<string> UploadAsyncActivity(IFileListEntry fileEntry)
        {
            var path = Path.Combine(_environment.ContentRootPath, "wwwroot\\images", fileEntry.Name);
            var ms = new MemoryStream();
            await fileEntry.Data.CopyToAsync(ms);
            using (FileStream file = new FileStream(path, FileMode.Create, FileAccess.Write))
            {
                ms.WriteTo(file);
            }
            ms.Dispose();
            ms.Close();
            System.Drawing.Image img1 = System.Drawing.Image.FromFile(path);
            //if (img1.Width > 600 || img1.Height > 600)
            if (img1.Width > 600 || img1.Height > 600)
            {
                img1.Dispose();

                return "1";
            }
            else
            {
                img1.Dispose();

                // File.Delete(Path.Combine(_environment.ContentRootPath, "wwwroot\\images", fileEntry.Name));
                return "0";
            }
           

        }


        public async Task UploadAsync(IFileListEntry fileEntry, string pathDir)
        {
            var path = Path.Combine(pathDir, "terms", fileEntry.Name);
            var ms = new MemoryStream();
            await fileEntry.Data.CopyToAsync(ms);
            using (FileStream file = new FileStream(path, FileMode.Create, FileAccess.Write))
            {
                ms.WriteTo(file);
            }
        }

        //public async Task getCropimg(string filname)
        //{
        //    var filePath = Path.Combine(_environment.ContentRootPath, "wwwroot\\images", filname);
        //    if (File.Exists(filePath))
        //    {
        //        System.Drawing.Image orgImg = System.Drawing.Image.FromFile(filePath);
        //        Rectangle CropArea = new Rectangle(
        //            Convert.ToInt32(X.Value),
        //            Convert.ToInt32(Y.Value),
        //            Convert.ToInt32(W.Value),
        //            Convert.ToInt32(H.Value));
        //    }
        //}
    }
}
