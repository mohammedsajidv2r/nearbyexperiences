﻿using NE_BaseObject;
using NE_BaseObject.Api;
using System;
using System.Collections.Generic;
using System.Text;

namespace NE_DA_Interface.Provider
{
    public interface IProviderDA
    {
        ResultObject<List<ProviderData>> getProvider(string RequestType, string param1, string param2);

        ResultObject<List<ProviderData>> getProviderCity(string RequestType, string param1, string param2, string city);

    }
}
