﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace NE_BaseObject.Admin
{
    public class TagsMasterData
    {
        public int tag_id { get; set; }

        [Required(ErrorMessage = "Please Enter Tag")]
        [MaxLength(100, ErrorMessage = "Tag name should be upto 100 characters.")]
        public string tags_name { get; set; }

        public DateTime? created_at { get; set; }

        public bool active { get; set; }
    }
}
