﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace NE_BaseObject.Admin
{
    public class ActivitIncludeData
    {
        public int include_id { get; set; }

        public string include_desc { get; set; }

        public bool? active { get; set; }

        public int? activity_master_id { get; set; }

        public DateTime? created_at { get; set; }

        public int incid { get; set; }
    }
}
