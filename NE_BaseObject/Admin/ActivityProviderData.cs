﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace NE_BaseObject.Admin
{
    public class ActivityProviderData
    {
        public int activity_provider_id { get; set; }

        [Required(ErrorMessage = "Please Enter First Name")]
        [MaxLength(100, ErrorMessage = "First Name should be upto 100 characters.")]
        public string first_name { get; set; }

        [Required(ErrorMessage = "Please Enter Last Name")]
        [MaxLength(100, ErrorMessage = "Last Name should be upto 100 characters.")]
        public string last_name { get; set; }


        [Required(ErrorMessage = "Please Enter Email")]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$", ErrorMessage = "Please enter valid email address")]
        [MaxLength(100, ErrorMessage = "Email should be upto 100 characters.")]
        public string email { get; set; }


        //[MinLength(6, ErrorMessage = "Password should contain atleast 6 characters.")]
        //[Required(ErrorMessage = "Please Enter Password")]
        //[MaxLength(100, ErrorMessage = "Password should be upto 100 characters.")]
        public string password { get; set; }

        [Required(ErrorMessage = "Please Enter Location")]
        [MaxLength(200, ErrorMessage = "Location should be upto 200 characters.")]
        public string location { get; set; }


        [Required(ErrorMessage = "Please Upload Image")]
        public string profile_picture { get; set; }

        [Required(ErrorMessage = "Please Select Star Rating")]
        public int? star_rating { get; set; }

        [Required(ErrorMessage = "Please Enter About Me")]
        [MaxLength(4000, ErrorMessage = "About Me should be upto 4000 characters.")]
        public string about_me { get; set; }

        [Required(ErrorMessage = "Please Enter Page Meta Title")]
        [MaxLength(255, ErrorMessage = "Meta Title should be upto 255 characters.")]
        public string metatitle { get; set; }

        [MaxLength(500, ErrorMessage = "Meta Description should be upto 500 characters.")]
        public string metadescription { get; set; }

        [MaxLength(255, ErrorMessage = "Meta Keyword should be upto 255 characters.")]
        public string metakeywords { get; set; }

        public DateTime? created_at { get; set; }

        public bool active { get; set; }

        public string slug_url { get; set; }

        [Required(ErrorMessage = "Please Enter Profession")]
        [MaxLength(200, ErrorMessage = "Profession should be upto 200 characters.")]
        public string profession { get; set; }
    }
}
