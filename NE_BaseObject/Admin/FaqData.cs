﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace NE_BaseObject.Admin
{
    public class FaqData
    {
        public int faq_id { get; set; }

        [Required(ErrorMessage = "Please Enter Question")]
        [MaxLength(200, ErrorMessage = "Question should be upto 200 characters.")]
        public string faq_question { get; set; }


        //[Required(ErrorMessage = "Please Enter Answer")]
        //[MaxLength(3000, ErrorMessage = "Answer should be upto 3000 characters.")]
        public string faq_answer { get; set; }

        public bool active { get; set; }
        public int RowNo { get; set; }
    }
}
