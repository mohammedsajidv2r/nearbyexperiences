﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace NE_BaseObject.Admin
{
    public class TestimonialData
    {
        public int testimonial_id { get; set; }

        [Required(ErrorMessage = "Please Enter Name")]
        [MaxLength(100, ErrorMessage = "Name should be upto 100 characters.")]
        public string testimonial_name { get; set; }

        [Required(ErrorMessage = "Please Enter Description")]
        [MaxLength(1000, ErrorMessage = "Description should be upto 1000 characters.")]
        public string testimonial_description { get; set; }

        [Required(ErrorMessage = "Please Upload Testimonial Image")]
        public string testimonial_image { get; set; }

        public bool active { get; set; }

        public DateTime? created_at { get; set; }

        [Required(ErrorMessage = "Please Enter Designation")]
        [MaxLength(100, ErrorMessage = "Designation should be upto 100 characters.")]
        public string designation { get; set; }
    }
}
