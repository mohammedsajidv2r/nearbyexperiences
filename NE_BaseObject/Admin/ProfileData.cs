﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace NE_BaseObject.Admin
{
    public class ProfileData
    {
        public int id { get; set; }

        public string password { get; set; }

        [Required(ErrorMessage = "Please Enter Name")]
        [MaxLength(100, ErrorMessage = "Name should be upto 100 characters.")]
        public string name { get; set; }

        [Required(ErrorMessage = "Please Enter Email")]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$", ErrorMessage = "Please enter valid email address")]
        [MaxLength(100, ErrorMessage = "Email should be upto 100 characters.")]
        public string email { get; set; }

        public DateTime? created_at { get; set; }

        [Required(ErrorMessage = "Please enter mobile number.")]
        [RegularExpression(@"^([0-9]{10})$", ErrorMessage = "Mobile number should contain 10 digits.")]
        public string phone_no { get; set; }
    }
}
