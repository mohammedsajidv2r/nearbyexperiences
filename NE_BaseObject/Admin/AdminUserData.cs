﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace NE_BaseObject.Admin
{
    public class AdminUserData
    {
        public int id { get; set; }

        [Required(ErrorMessage = "Please Enter Password")]
        [MinLength(6, ErrorMessage = "Password should contain atleast 6 characters.")]
        public string password { get; set; }
        public string name { get; set; }

        [Required(ErrorMessage = "Please Enter Email")]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$", ErrorMessage = "Please enter valid email address")]
        public string email { get; set; }

        public DateTime? created_at { get; set; }

        public string phone_no { get; set; }

        //[TempData]
        //public string AdminaName { get; set; }
    }
}
