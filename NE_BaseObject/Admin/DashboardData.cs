﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NE_BaseObject.Admin
{
    public class DashboardData
    {
        public string cityCount { get; set; }
        public string tagCount { get; set; }
        public string providerCount { get; set; }
        public string activityCount { get; set; }

        public string providerChart { get; set; }
        public string activityChart { get; set; }
    }
}
