﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace NE_BaseObject.Admin
{
    public class SignupRequestData
    {
        public int signup_req_id { get; set; }

        [Required(ErrorMessage = "Please Enter Name")]
        public string name { get; set; }

        [Required(ErrorMessage = "Please Enter Email")]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$", ErrorMessage = "Please enter valid email address")]
        public string email { get; set; }

        [Required(ErrorMessage = "Please Enter Location")]
        public string subject { get; set; }

        [Required(ErrorMessage = "Please Enter Message")]
        public string message { get; set; }

        public DateTime? created_at { get; set; }
    }
}
