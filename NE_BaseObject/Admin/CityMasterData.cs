﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace NE_BaseObject.Admin
{
    public class CityMasterData
    {
        public int city_id { get; set; }

        [Required(ErrorMessage = "Please Enter City")]

        [MaxLength(100, ErrorMessage = "City name should be upto 100 characters.")]
        public string city_name { get; set; }

        public DateTime? created_at { get; set; }

        public bool active { get; set; }

        public string slug_url { get; set; }

    }
}
