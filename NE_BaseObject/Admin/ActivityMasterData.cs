﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace NE_BaseObject.Admin
{
    public class ActivityMasterData
    {

        public int activity_master_id { get; set; }

        [Required(ErrorMessage = "Please Select Activity Provider")]
        public int? activity_provider_id { get; set; }

        [Required(ErrorMessage = "Please Select Tag")]
        public int? tag_id { get; set; }

        [Required(ErrorMessage = "Please Select City")]
        public int? city_id { get; set; }

        [Required(ErrorMessage = "Please Enter Activity Name")]
        [MaxLength(100, ErrorMessage = "Activity name should be upto 100 characters.")]
        public string title { get; set; }
        public string description { get; set; }

        [Required(ErrorMessage = "Please Enter Price")]
        //[RegularExpression(@"^\d+.\d{0,12}$", ErrorMessage = "Price should be upto 12 digit.")]
        //[MaxLength(12, ErrorMessage = "Price should be upto 12 digit.")]
        public decimal? price { get; set; }

        [Required(ErrorMessage = "Please Upload Image")]
        public string picture { get; set; }

        [Required(ErrorMessage = "Please Enter Widget Code")]
        [MaxLength(2000, ErrorMessage = "Widget Code should be upto 2000 characters.")]
        public string widget_code { get; set; }


        [Required(ErrorMessage = "Please Enter Page Meta Title")]
        [MaxLength(255, ErrorMessage = "Meta Title should be upto 255 characters.")]
        public string metatitle { get; set; }

        [MaxLength(500, ErrorMessage = "Meta Description should be upto 500 characters.")]
        public string metadescription { get; set; }

        [MaxLength(255, ErrorMessage = "Meta Keyword should be upto 255 characters.")]
        public string metakeywords { get; set; }

        public DateTime? created_at { get; set; }

        public bool most_popular { get; set; }

        public bool featured { get; set; }

        [Required(ErrorMessage = "Please Enter Location")]
        [MaxLength(200, ErrorMessage = "Location should be upto 200 characters.")]
        public string location { get; set; }

        public string highlight { get; set; }

        public bool active { get; set; }

        [Required(ErrorMessage = "Please Enter Short Descrption")]
        [MaxLength(200, ErrorMessage = "Short Descrption should be upto 200 characters.")]
        public string short_description { get; set; }

        public string providername { get; set; }

        public string slug_url { get; set; }

        [Required(ErrorMessage = "Please Select Widget Type")]
        public int? widget_type { get; set; }

        public string donation { get; set; }
    }
}
