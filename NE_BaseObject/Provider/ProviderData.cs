﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace NE_BaseObject.Api
{
    public class ProviderData
    {
        public int activity_master_id { get; set; }

        public int? activity_provider_id { get; set; }

        public string first_name { get; set; }
        public string last_name { get; set; }
        public string location { get; set; }

        public string title { get; set; }

        public string description { get; set; }

        public decimal? price { get; set; }

        public string providerpicture { get; set; }
        public string profilepicture { get; set; }

        public string highlight { get; set; }
        public int star_rating { get; set; }

        public string short_description { get; set; }

        public string city_id { get; set; }
         
        public string metatitle { get; set; }
        public string metadescription { get; set; }
        public string metakeywords { get; set; }

        public string tags_name { get; set; }
        public string slug_url { get; set; }

        public string city_slug_url { get; set; }

        public string provider_slug_url { get; set; }

        public string profession { get; set; }

        public string widget_code { get; set; }
        public int? widget_type { get; set; }

        public string donation { get; set; }
    }
}
