﻿using NE_Utilities.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace NE_BaseObject
{
    public class ResultObject<T>
    {
        public ResultType Result;
        public string ResultMessage;
        public string Remark;
        public T ResultData;
    }
}
