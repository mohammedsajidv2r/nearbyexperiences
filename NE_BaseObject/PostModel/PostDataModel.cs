﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NE_BaseObject.PostModel
{
    public class PostDataModel
    {
        public class HomePostData
        {
            public string param { get; set; }
        }

        public class EncryptPostData
        {
            public string hdndata { get; set; }
        }

        public class expolorActivity
        {
            public string cityId { get; set; }
            public string section { get; set; }
            public string slugKey { get; set; }
            public string city { get; set; }

        }

        public class activity
        {
            //public string activitymasterid { get; set; }
            public string activityId { get; set; }
            public string providerId { get; set; }
            public string slugKey { get; set; }

            public string term { get; set; }
        }

        public class signupForm
        {
            public string fullName { get; set; }
            public string email { get; set; }
            public string subject { get; set; }
            public string message { get; set; }
        }
    }
}
