﻿using NE_BaseObject;
using NE_BaseObject.Admin;
using System;
using System.Collections.Generic;
using System.Text;

namespace NE_Mgr_Interface.Admin
{
    public interface IAdminMgr
    {
        ResultObject<AdminUserData> adminLoginAuth(string RequestType, AdminUserData data);
        ResultObject<List<CityMasterData>> cityMaster(string RequestType, string param1, string param2);

        ResultObject<string> editCityMaster(string RequestType, string param, CityMasterData data);

        ResultObject<List<TagsMasterData>> tagMaster(string RequestType, string param1, string param2);

        ResultObject<string> editTagMaster(string RequestType, string param, TagsMasterData data);

        ResultObject<List<CmsPageData>> cmsMaster(string RequestType, string param1, string param2);

        ResultObject<string> editCmsMaster(string RequestType, string param, CmsPageData data);

        ResultObject<List<SignupRequestData>> signrequestMaster(string RequestType, string param1, string param2);

        ResultObject<string> editSignMaster(string RequestType, string param, SignupRequestData data);

        ResultObject<List<ActivityProviderData>> acitvityProvider(string RequestType, string param1, string param2);

        ResultObject<string> editacitvityProvider(string RequestType, string param, ActivityProviderData data);

        ResultObject<List<ActivityMasterData>> acitvityMaster(string RequestType, string param1, string param2);

        ResultObject<string> editacitvityMaster(string RequestType, string param, ActivityMasterData data);

        ResultObject<string> editacitvityMasterWithInclude(string RequestType, string param, ActivityMasterData data, ActivitIncludeData data1);

        ResultObject<List<ActivitIncludeData>> activityInclude(string RequestType, string param1, string param2);

        ResultObject<string> editacitvityMasterWithIncludeList(string RequestType, string param, ActivityMasterData data, List<ActivitIncludeData> data1);

        ResultObject<ProfileData> getProfileData(string RequestType, string param1, string param2);
        ResultObject<string> updateProfile(string RequestType, string param1, string param2, ProfileData data);

        ResultObject<List<TestimonialData>> tetimonial(string RequestType, string param1, string param2);

        ResultObject<string> edittetimonial(string RequestType, string param, TestimonialData data);

        ResultObject<string> resetpassword(string RequestType, string param1, string param2);

        ResultObject<List<ConfigurationData>> viewConfiguration(string RequestType, string param1, string param2);

        ResultObject<string> editConfiguration(string RequestType, string param, ConfigurationData data);
        ResultObject<DashboardData> dashboardData(string RequestType, string param1, string param2);

        ResultObject<List<FaqData>> viewfaq(string RequestType, string param1, string param2);

        ResultObject<string> editfaq(string RequestType, string param, FaqData data);

        ResultObject<string> updateresetpass(string RequestType, string param1, string param2, string param3);
    }
}
