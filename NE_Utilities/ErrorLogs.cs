﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace NE_Utilities
{
    public class ErrorLogs
    {
        public static bool _InsertLogs(string RequestType, string Name, string Type, int? ErrorLine, string Error_message)
        {
            //SqlConnection con = new SqlConnection(@"Data Source=nifty-database-ms-sql.cguyphbsr0iw.ap-south-1.rds.amazonaws.com;Initial Catalog=niftytrader;User ID=niftymssql;Password=niftymssql4862!");
              SqlConnection con1 = new SqlConnection(@"Data Source=DESKTOP-V6QNKOK\\SQLEXPRESS01;Initial Catalog=NearbyExperiences;User ID=sa;Password=1234");
            try
            {
                SqlCommand cmd = new SqlCommand("USP_Logs", con1);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Request_type", RequestType);
                cmd.Parameters.AddWithValue("@Name", Name);
                cmd.Parameters.AddWithValue("@Type", Type);
                cmd.Parameters.AddWithValue("@Error_Line", ErrorLine);
                cmd.Parameters.AddWithValue("@ErrorMsg", Error_message);
                con1.Open();
                cmd.ExecuteNonQuery();
                con1.Close();
            }
            catch (Exception ex)
            {
                con1.Close();
            }
            return true;
        }
    }
}
