﻿using AutoMapper;
using NE_BaseObject;
using NE_BaseObject.Admin;
using NE_DA_Interface.Admin;
using NE_Mgr_Interface.Admin;
using System;
using System.Collections.Generic;
using System.Text;

namespace NE_Mgr.Admin
{
    public  class AdminMgr : IAdminMgr
    {
        IAdminDA _IAdminDA;
        IMapper _mapper;

        public AdminMgr(IMapper IMapper, IAdminDA IAdminDA)
        {
            _IAdminDA = IAdminDA;
            _mapper = IMapper;
        }
        public ResultObject<AdminUserData> adminLoginAuth(string RequestType, AdminUserData data)
        {
            ResultObject<AdminUserData> resultObject = new ResultObject<AdminUserData>();
            try
            {
                resultObject = _mapper.Map<ResultObject<AdminUserData>>(_IAdminDA.adminLoginAuth(RequestType, data));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "adminLoginAuth", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<List<CityMasterData>> cityMaster(string RequestType, string param1, string param2)
        {
            ResultObject<List<CityMasterData>> resultObject = new ResultObject<List<CityMasterData>>();
            try
            {
                resultObject = _mapper.Map<ResultObject<List<CityMasterData>>>(_IAdminDA.cityMaster(RequestType, param1, param2));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "cityMaster", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<string> editCityMaster(string RequestType, string param, CityMasterData data)
        {
            ResultObject<string> resultObject = new ResultObject<string>();
            try
            {
                resultObject = _mapper.Map<ResultObject<string>>(_IAdminDA.editCityMaster(RequestType, param, data));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "editCityMaster", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }


        public ResultObject<string> editTagMaster(string RequestType, string param, TagsMasterData data)
        {
            ResultObject<string> resultObject = new ResultObject<string>();
            try
            {
                resultObject = _mapper.Map<ResultObject<string>>(_IAdminDA.editTagMaster(RequestType, param, data));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "editTagMaster", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<List<TagsMasterData>> tagMaster(string RequestType, string param1, string param2)
        {
            ResultObject<List<TagsMasterData>> resultObject = new ResultObject<List<TagsMasterData>>();
            try
            {
                resultObject = _mapper.Map<ResultObject<List<TagsMasterData>>>(_IAdminDA.tagMaster(RequestType, param1, param2));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "cityMaster", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }


        public ResultObject<List<CmsPageData>> cmsMaster(string RequestType, string param1, string param2)
        {
            ResultObject<List<CmsPageData>> resultObject = new ResultObject<List<CmsPageData>>();
            try
            {
                resultObject = _mapper.Map<ResultObject<List<CmsPageData>>>(_IAdminDA.cmsMaster(RequestType, param1, param2));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "cmsMaster", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<string> editCmsMaster(string RequestType, string param, CmsPageData data)
        {
            ResultObject<string> resultObject = new ResultObject<string>();
            try
            {
                resultObject = _mapper.Map<ResultObject<string>>(_IAdminDA.editCmsMaster(RequestType, param, data));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "editCmsMaster", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }


        public ResultObject<List<SignupRequestData>> signrequestMaster(string RequestType, string param1, string param2)
        {
            ResultObject<List<SignupRequestData>> resultObject = new ResultObject<List<SignupRequestData>>();
            try
            {
                resultObject = _mapper.Map<ResultObject<List<SignupRequestData>>>(_IAdminDA.signrequestMaster(RequestType, param1, param2));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "cmsMaster", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<string> editSignMaster(string RequestType, string param, SignupRequestData data)
        {
            ResultObject<string> resultObject = new ResultObject<string>();
            try
            {
                resultObject = _mapper.Map<ResultObject<string>>(_IAdminDA.editSignMaster(RequestType, param, data));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "editSignMaster", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }


        public ResultObject<List<ActivityProviderData>> acitvityProvider(string RequestType, string param1, string param2)
        {
            ResultObject<List<ActivityProviderData>> resultObject = new ResultObject<List<ActivityProviderData>>();
            try
            {
                resultObject = _mapper.Map<ResultObject<List<ActivityProviderData>>>(_IAdminDA.acitvityProvider(RequestType, param1, param2));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "acitvityProvider", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<string> editacitvityProvider(string RequestType, string param, ActivityProviderData data)
        {
            ResultObject<string> resultObject = new ResultObject<string>();
            try
            {
                resultObject = _mapper.Map<ResultObject<string>>(_IAdminDA.editacitvityProvider(RequestType, param, data));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "editacitvityProvider", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }


        public ResultObject<List<ActivityMasterData>> acitvityMaster(string RequestType, string param1, string param2)
        {
            ResultObject<List<ActivityMasterData>> resultObject = new ResultObject<List<ActivityMasterData>>();
            try
            {
                resultObject = _mapper.Map<ResultObject<List<ActivityMasterData>>>(_IAdminDA.acitvityMaster(RequestType, param1, param2));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "acitvityMaster", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<string> editacitvityMaster(string RequestType, string param, ActivityMasterData data)
        {
            ResultObject<string> resultObject = new ResultObject<string>();
            try
            {
                resultObject = _mapper.Map<ResultObject<string>>(_IAdminDA.editacitvityMaster(RequestType, param, data));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "editacitvityMaster", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<string> editacitvityMasterWithInclude(string RequestType, string param, ActivityMasterData data, ActivitIncludeData data1)
        {
            ResultObject<string> resultObject = new ResultObject<string>();
            try
            {
                resultObject = _mapper.Map<ResultObject<string>>(_IAdminDA.editacitvityMasterWithInclude(RequestType, param, data, data1));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "editacitvityMasterWithInclude", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<List<ActivitIncludeData>> activityInclude(string RequestType, string param1, string param2)
        {
            ResultObject<List<ActivitIncludeData>> resultObject = new ResultObject<List<ActivitIncludeData>>();
            try
            {
                resultObject = _mapper.Map<ResultObject<List<ActivitIncludeData>>>(_IAdminDA.activityInclude(RequestType, param1, param2));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "activityInclude", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<string> editacitvityMasterWithIncludeList(string RequestType, string param, ActivityMasterData data, List<ActivitIncludeData> data1)
        {
            ResultObject<string> resultObject = new ResultObject<string>();
            try
            {
                resultObject = _mapper.Map<ResultObject<string>>(_IAdminDA.editacitvityMasterWithIncludeList(RequestType, param, data, data1));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "editacitvityMasterWithIncludeList", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<ProfileData> getProfileData(string RequestType, string param1, string param2)
        {
            ResultObject<ProfileData> resultObject = new ResultObject<ProfileData>();
            try
            {
                resultObject = _mapper.Map<ResultObject<ProfileData>>(_IAdminDA.getProfileData(RequestType, param1, param2));
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "getProfileData", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<string> updateProfile(string RequestType, string param1, string param2, ProfileData data)
        {
             ResultObject<string> resultObject = new ResultObject<string>();
            try
            {
                resultObject = _mapper.Map<ResultObject<string>>(_IAdminDA.updateProfile(RequestType, param1, param2, data));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "updateProfile", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<List<TestimonialData>> tetimonial(string RequestType, string param1, string param2)
        {
            ResultObject<List<TestimonialData>> resultObject = new ResultObject<List<TestimonialData>>();
            try
            {
                resultObject = _mapper.Map<ResultObject<List<TestimonialData>>>(_IAdminDA.tetimonial(RequestType, param1, param2));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "tetimonial", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<string> edittetimonial(string RequestType, string param, TestimonialData data)
        {
            ResultObject<string> resultObject = new ResultObject<string>();
            try
            {
                resultObject = _mapper.Map<ResultObject<string>>(_IAdminDA.edittetimonial(RequestType, param,  data));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "edittetimonial", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<string> resetpassword(string RequestType, string param1, string param2)
        {
            ResultObject<string> resultObject = new ResultObject<string>();
            try
            {
                resultObject = _mapper.Map<ResultObject<string>>(_IAdminDA.resetpassword(RequestType, param1, param2));
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "resetpassword", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<List<ConfigurationData>> viewConfiguration(string RequestType, string param1, string param2)
        {
            ResultObject<List<ConfigurationData>> resultObject = new ResultObject<List<ConfigurationData>>();
            try
            {
                resultObject = _mapper.Map<ResultObject<List<ConfigurationData>>>(_IAdminDA.viewConfiguration(RequestType, param1, param2));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "tetimonial", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<string> editConfiguration(string RequestType, string param, ConfigurationData data)
        {
            ResultObject<string> resultObject = new ResultObject<string>();
            try
            {
                resultObject = _mapper.Map<ResultObject<string>>(_IAdminDA.editConfiguration(RequestType, param, data));
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "editConfiguration", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<DashboardData> dashboardData(string RequestType, string param1, string param2)
        {
            ResultObject<DashboardData> resultObject = new ResultObject<DashboardData>();
            try
            {
                resultObject = _mapper.Map<ResultObject<DashboardData>>(_IAdminDA.dashboardData(RequestType, param1, param2));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "tetimonial", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<List<FaqData>> viewfaq(string RequestType, string param1, string param2)
        {
            ResultObject<List<FaqData>> resultObject = new ResultObject<List<FaqData>>();
            try
            {
                resultObject = _mapper.Map<ResultObject<List<FaqData>>>(_IAdminDA.viewfaq(RequestType, param1, param2));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "tetimonial", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<string> editfaq(string RequestType, string param, FaqData data)
        {
            ResultObject<string> resultObject = new ResultObject<string>();
            try
            {
                resultObject = _mapper.Map<ResultObject<string>>(_IAdminDA.editfaq(RequestType, param, data));
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "editConfiguration", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<string> updateresetpass(string RequestType, string param1, string param2, string param3)
        {
            ResultObject<string> resultObject = new ResultObject<string>();
            try
            {
                resultObject = _mapper.Map<ResultObject<string>>(_IAdminDA.updateresetpass(RequestType, param1, param2, param3));
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "editConfiguration", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }
    }
}
