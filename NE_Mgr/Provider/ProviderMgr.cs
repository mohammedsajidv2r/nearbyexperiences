﻿using AutoMapper;
using NE_BaseObject;
using NE_BaseObject.Api;
using NE_DA_Interface.Provider;
using NE_Mgr_Interface.Provider;
using System;
using System.Collections.Generic;
using System.Text;

namespace NE_Mgr.Provider
{
   public class ProviderMgr : IProviderMgr
    {
        IProviderDA _IProviderDA;
        IMapper _mapper;
        public ProviderMgr(IMapper IMapper, IProviderDA IProviderDA)
        {
            _IProviderDA = IProviderDA;
            _mapper = IMapper;
        }

        public ResultObject<List<ProviderData>> getProvider(string RequestType, string param1, string param2)
        {
            ResultObject<List<ProviderData>> resultObject = new ResultObject<List<ProviderData>>();
            try
            {
                resultObject = _mapper.Map<ResultObject<List<ProviderData>>>(_IProviderDA.getProvider(RequestType, param1, param2));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "getProvider", "IProviderMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<List<ProviderData>> getProviderCity(string RequestType, string param1, string param2, string city)
        {
            ResultObject<List<ProviderData>> resultObject = new ResultObject<List<ProviderData>>();
            try
            {
                resultObject = _mapper.Map<ResultObject<List<ProviderData>>>(_IProviderDA.getProviderCity(RequestType, param1, param2,city));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "getProvider", "IProviderMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }
    }
}
