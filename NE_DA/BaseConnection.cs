﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace NE_DA
{
    public class BaseConnection : IDisposable
    {
        protected IDbConnection con;
        public IHttpContextAccessor _httpContextAccessor;

        public BaseConnection(IConfiguration config, IHttpContextAccessor httpContextAccessor = null)
        {
            var conn = config.GetConnectionString("NearByExperiencesDB");
            con = new SqlConnection(conn);

            _httpContextAccessor = httpContextAccessor;
        }

        public BaseConnection(IConfiguration config, IHttpContextAccessor httpContextAccessor = null, string ConnectionName = "")
        {
        }

        public void Dispose()
        {

        }
    }
}
