﻿using Dapper;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using NE_BaseObject;
using NE_BaseObject.Api;
using NE_DA_Interface.Provider;
using NE_Utilities.Enum;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace NE_DA.Provider
{
    public class ProviderDA : BaseConnection, IProviderDA
    {
        public ProviderDA(IConfiguration config, IHttpContextAccessor httpContextAccessor) : base(config, httpContextAccessor)
        {

        }
        public ResultObject<List<ProviderData>> getProvider(string RequestType, string param1, string param2)
        {
            ResultObject<List<ProviderData>> resPlan = new ResultObject<List<ProviderData>>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@param", param1);
                parameters.Add("@section", param2);
                resPlan.ResultData = SqlMapper.Query<ProviderData>(con, "USP_Provider", param: parameters, commandType: CommandType.StoredProcedure).ToList();

                if (resPlan.ResultData == null || resPlan.ResultData.Count <= 0)
                {
                    resPlan.ResultData = new List<ProviderData>();
                    resPlan.ResultMessage = "Data Not Found.";
                    resPlan.Result = ResultType.Info;
                }
                else
                {
                    resPlan.ResultMessage = "Success";
                    resPlan.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "getProvider", "ProviderDA", 0, ex.Message.ToString());
                resPlan.ResultMessage = ex.Message.ToString();
                resPlan.Result = ResultType.Error;
            }
            return resPlan;
        }

        public ResultObject<List<ProviderData>> getProviderCity(string RequestType, string param1, string param2, string city)
        {
            ResultObject<List<ProviderData>> resPlan = new ResultObject<List<ProviderData>>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@param", param1);
                parameters.Add("@section", param2);
                parameters.Add("@city", city);
                resPlan.ResultData = SqlMapper.Query<ProviderData>(con, "USP_Provider", param: parameters, commandType: CommandType.StoredProcedure).ToList();

                if (resPlan.ResultData == null || resPlan.ResultData.Count <= 0)
                {
                    resPlan.ResultData = new List<ProviderData>();
                    resPlan.ResultMessage = "Data Not Found.";
                    resPlan.Result = ResultType.Info;
                }
                else
                {
                    resPlan.ResultMessage = "Success";
                    resPlan.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "getProvider", "ProviderDA", 0, ex.Message.ToString());
                resPlan.ResultMessage = ex.Message.ToString();
                resPlan.Result = ResultType.Error;
            }
            return resPlan;
        }
    }
}
