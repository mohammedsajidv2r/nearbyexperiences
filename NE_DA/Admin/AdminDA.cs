﻿using Dapper;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using NE_BaseObject;
using NE_BaseObject.Admin;
using NE_DA_Interface.Admin;
using NE_Utilities;
using NE_Utilities.Enum;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace NE_DA.Admin
{
    public class AdminDA : BaseConnection, IAdminDA
    {
        public AdminDA(IConfiguration config, IHttpContextAccessor httpContextAccessor) : base(config, httpContextAccessor)
        {

        }
        public ResultObject<AdminUserData> adminLoginAuth(string RequestType, AdminUserData data)
        {
            ResultObject<AdminUserData> resQuery = new ResultObject<AdminUserData>();
            try
            {
                //encrypt
                data.password = Encryption.EncryptString(data.password, Encryption.passkey);
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@email", data.email);
                parameters.Add("@password", data.password);
                resQuery.ResultData = SqlMapper.Query<AdminUserData>(con, "USP_Admin", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();

                if (resQuery.ResultData == null)
                {
                    resQuery.ResultData = new AdminUserData();
                    resQuery.ResultMessage = "Data Not Found.";
                    resQuery.Result = ResultType.Info;
                }
                else
                {
                    resQuery.ResultMessage = "Success";
                    resQuery.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "loginAuth", "AdminDA", 0, ex.Message.ToString());
                resQuery.ResultMessage = ex.Message.ToString();
                resQuery.Result = ResultType.Error;
            }
            return resQuery;
        }

        public ResultObject<List<CityMasterData>> cityMaster(string RequestType, string param1, string param2)
        {
            ResultObject<List<CityMasterData>> resPlan = new ResultObject<List<CityMasterData>>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@id", param1);
                resPlan.ResultData = SqlMapper.Query<CityMasterData>(con, "USP_Admin", param: parameters, commandType: CommandType.StoredProcedure).ToList();

                if (resPlan.ResultData == null || resPlan.ResultData.Count <= 0)
                {
                    resPlan.ResultData = new List<CityMasterData>();
                    resPlan.ResultMessage = "Data Not Found.";
                    resPlan.Result = ResultType.Info;
                }
                else
                {
                    resPlan.ResultMessage = "Success";
                    resPlan.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "cityMaster", "AdminDA", 0, ex.Message.ToString());
                resPlan.ResultMessage = ex.Message.ToString();
                resPlan.Result = ResultType.Error;
            }
            return resPlan;
        }

        public ResultObject<string> editCityMaster(string RequestType, string param, CityMasterData data)
        {
            ResultObject<string> resPlan = new ResultObject<string>();
            try
            {
                var XMLData = XmlConversion.SerializeToXElement(data);
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@XmlInput", XMLData);
                parameters.Add("@id", param);
                string _Result = SqlMapper.Query<string>(con, "USP_Admin", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (_Result == "Success")
                {
                    resPlan.ResultData = _Result.ToString();
                    resPlan.Result = ResultType.Success;
                    resPlan.ResultMessage = "Success";
                }
                else if (_Result == "City Already Exists")
                {
                    resPlan.ResultData = _Result.ToString();
                    resPlan.Result = ResultType.Success;
                    resPlan.ResultMessage = _Result;
                }
                else
                {
                    resPlan.ResultMessage = "Data Not Found.";
                    resPlan.Result = ResultType.Info;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "editCityMaster", "AdminDA", 0, ex.Message.ToString());
                resPlan.ResultMessage = ex.Message.ToString();
                resPlan.Result = ResultType.Error;
            }
            return resPlan;
        }



        public ResultObject<string> editTagMaster(string RequestType, string param, TagsMasterData data)
        {
            ResultObject<string> resPlan = new ResultObject<string>();
            try
            {
                var XMLData = XmlConversion.SerializeToXElement(data);
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@XmlInput", XMLData);
                parameters.Add("@id", param);
                string _Result = SqlMapper.Query<string>(con, "USP_Admin", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (_Result == "Success")
                {
                    resPlan.ResultData = _Result.ToString();
                    resPlan.Result = ResultType.Success;
                    resPlan.ResultMessage = "Success";
                }
                else if (_Result == "Tag Already Exists")
                {
                    resPlan.ResultData = _Result.ToString();
                    resPlan.Result = ResultType.Success;
                    resPlan.ResultMessage = _Result;
                }
                else
                {
                    resPlan.ResultMessage = "Data Not Found.";
                    resPlan.Result = ResultType.Info;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "editTagMaster", "AdminDA", 0, ex.Message.ToString());
                resPlan.ResultMessage = ex.Message.ToString();
                resPlan.Result = ResultType.Error;
            }
            return resPlan;
        }

        public ResultObject<List<TagsMasterData>> tagMaster(string RequestType, string param1, string param2)
        {
            ResultObject<List<TagsMasterData>> resPlan = new ResultObject<List<TagsMasterData>>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                resPlan.ResultData = SqlMapper.Query<TagsMasterData>(con, "USP_Admin", param: parameters, commandType: CommandType.StoredProcedure).ToList();

                if (resPlan.ResultData == null || resPlan.ResultData.Count <= 0)
                {
                    resPlan.ResultData = new List<TagsMasterData>();
                    resPlan.ResultMessage = "Data Not Found.";
                    resPlan.Result = ResultType.Info;
                }
                else
                {
                    resPlan.ResultMessage = "Success";
                    resPlan.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "tagMaster", "AdminDA", 0, ex.Message.ToString());
                resPlan.ResultMessage = ex.Message.ToString();
                resPlan.Result = ResultType.Error;
            }
            return resPlan;
        }


        public ResultObject<string> editCmsMaster(string RequestType, string param, CmsPageData data)
        {
            ResultObject<string> resPlan = new ResultObject<string>();
            try
            {
                var XMLData = XmlConversion.SerializeToXElement(data);
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@XmlInput", XMLData);
                parameters.Add("@id", param);
                string _Result = SqlMapper.Query<string>(con, "USP_Admin", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (_Result == "Success")
                {
                    resPlan.ResultData = _Result.ToString();
                    resPlan.Result = ResultType.Success;
                    resPlan.ResultMessage = "Success";
                }
                else if (_Result == "CMS Title Already Exists")
                {
                    resPlan.ResultData = _Result.ToString();
                    resPlan.Result = ResultType.Success;
                    resPlan.ResultMessage = _Result;
                }
                else
                {
                    resPlan.ResultMessage = "Data Not Found.";
                    resPlan.Result = ResultType.Info;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "editTagMaster", "AdminDA", 0, ex.Message.ToString());
                resPlan.ResultMessage = ex.Message.ToString();
                resPlan.Result = ResultType.Error;
            }
            return resPlan;
        }

        public ResultObject<List<CmsPageData>> cmsMaster(string RequestType, string param1, string param2)
        {
            ResultObject<List<CmsPageData>> resPlan = new ResultObject<List<CmsPageData>>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@param", param1);
                resPlan.ResultData = SqlMapper.Query<CmsPageData>(con, "USP_Admin", param: parameters, commandType: CommandType.StoredProcedure).ToList();

                if (resPlan.ResultData == null || resPlan.ResultData.Count <= 0)
                {
                    resPlan.ResultData = new List<CmsPageData>();
                    resPlan.ResultMessage = "Data Not Found.";
                    resPlan.Result = ResultType.Info;
                }
                else
                {
                    resPlan.ResultMessage = "Success";
                    resPlan.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "cmsMaster", "AdminDA", 0, ex.Message.ToString());
                resPlan.ResultMessage = ex.Message.ToString();
                resPlan.Result = ResultType.Error;
            }
            return resPlan;
        }

        public ResultObject<List<SignupRequestData>> signrequestMaster(string RequestType, string param1, string param2)
        {
            ResultObject<List<SignupRequestData>> resPlan = new ResultObject<List<SignupRequestData>>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                resPlan.ResultData = SqlMapper.Query<SignupRequestData>(con, "USP_Admin", param: parameters, commandType: CommandType.StoredProcedure).ToList();

                if (resPlan.ResultData == null || resPlan.ResultData.Count <= 0)
                {
                    resPlan.ResultData = new List<SignupRequestData>();
                    resPlan.ResultMessage = "Data Not Found.";
                    resPlan.Result = ResultType.Info;
                }
                else
                {
                    resPlan.ResultMessage = "Success";
                    resPlan.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "signrequestMaster", "AdminDA", 0, ex.Message.ToString());
                resPlan.ResultMessage = ex.Message.ToString();
                resPlan.Result = ResultType.Error;
            }
            return resPlan;
        }

        public ResultObject<string> editSignMaster(string RequestType, string param, SignupRequestData data)
        {
            ResultObject<string> resPlan = new ResultObject<string>();
            try
            {
                var XMLData = XmlConversion.SerializeToXElement(data);
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@XmlInput", XMLData);
                parameters.Add("@id", param);
                string _Result = SqlMapper.Query<string>(con, "USP_Admin", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (_Result == "Success")
                {
                    resPlan.ResultData = _Result.ToString();
                    resPlan.Result = ResultType.Success;
                    resPlan.ResultMessage = "Success";
                }
                else if (_Result == "SignUp Name Already Exists")
                {
                    resPlan.ResultData = _Result.ToString();
                    resPlan.Result = ResultType.Success;
                    resPlan.ResultMessage = _Result;
                }
                else
                {
                    resPlan.ResultMessage = "Data Not Found.";
                    resPlan.Result = ResultType.Info;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "editSignMaster", "AdminDA", 0, ex.Message.ToString());
                resPlan.ResultMessage = ex.Message.ToString();
                resPlan.Result = ResultType.Error;
            }
            return resPlan;
        }

        public ResultObject<List<ActivityProviderData>> acitvityProvider(string RequestType, string param1, string param2)
        {
            ResultObject<List<ActivityProviderData>> resPlan = new ResultObject<List<ActivityProviderData>>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@param", param1);
                resPlan.ResultData = SqlMapper.Query<ActivityProviderData>(con, "USP_Admin_AcitvityProvider", param: parameters, commandType: CommandType.StoredProcedure).ToList();

                if (resPlan.ResultData == null || resPlan.ResultData.Count <= 0)
                {
                    resPlan.ResultData = new List<ActivityProviderData>();
                    resPlan.ResultMessage = "Data Not Found.";
                    resPlan.Result = ResultType.Info;
                }
                else
                {
                    resPlan.ResultMessage = "Success";
                    resPlan.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "acitvityProviderMaster", "AdminDA", 0, ex.Message.ToString());
                resPlan.ResultMessage = ex.Message.ToString();
                resPlan.Result = ResultType.Error;
            }
            return resPlan;
        }

        public ResultObject<string> editacitvityProvider(string RequestType, string param, ActivityProviderData data)
        {
            ResultObject<string> resPlan = new ResultObject<string>();
            try
            {
                var XMLData = XmlConversion.SerializeToXElement(data);
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@XmlInput", XMLData);
                parameters.Add("@id", param);
                string _Result = SqlMapper.Query<string>(con, "USP_Admin_AcitvityProvider", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (_Result == "Success")
                {
                    resPlan.ResultData = _Result.ToString();
                    resPlan.Result = ResultType.Success;
                    resPlan.ResultMessage = "Success";
                }
                else if (_Result == "Activity Provider Email Already Exists")
                {
                    resPlan.ResultData = _Result.ToString();
                    resPlan.Result = ResultType.Success;
                    resPlan.ResultMessage = _Result;
                }
                else
                {
                    resPlan.ResultMessage = "Data Not Found.";
                    resPlan.Result = ResultType.Info;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "editacitvityProvider", "AdminDA", 0, ex.Message.ToString());
                resPlan.ResultMessage = ex.Message.ToString();
                resPlan.Result = ResultType.Error;
            }
            return resPlan;
        }

        public ResultObject<List<ActivityMasterData>> acitvityMaster(string RequestType, string param1, string param2)
        {
            ResultObject<List<ActivityMasterData>> resPlan = new ResultObject<List<ActivityMasterData>>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@param", param1);
                resPlan.ResultData = SqlMapper.Query<ActivityMasterData>(con, "USP_Admin_AcitvityMaster", param: parameters, commandType: CommandType.StoredProcedure).ToList();

                if (resPlan.ResultData == null || resPlan.ResultData.Count <= 0)
                {
                    resPlan.ResultData = new List<ActivityMasterData>();
                    resPlan.ResultMessage = "Data Not Found.";
                    resPlan.Result = ResultType.Info;
                }
                else
                {
                    resPlan.ResultMessage = "Success";
                    resPlan.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "acitvityProviderMaster", "AdminDA", 0, ex.Message.ToString());
                resPlan.ResultMessage = ex.Message.ToString();
                resPlan.Result = ResultType.Error;
            }
            return resPlan;
        }

        public ResultObject<string> editacitvityMaster(string RequestType, string param, ActivityMasterData data)
        {
            ResultObject<string> resPlan = new ResultObject<string>();
            try
            {
                var XMLData = XmlConversion.SerializeToXElement(data);
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@XmlInput", XMLData);
                parameters.Add("@id", param);
                string _Result = SqlMapper.Query<string>(con, "USP_Admin_AcitvityMaster", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (_Result == "Success")
                {
                    resPlan.ResultData = _Result.ToString();
                    resPlan.Result = ResultType.Success;
                    resPlan.ResultMessage = "Success";
                }
                else if (_Result == "Activity Master Title Already Exists")
                {
                    resPlan.ResultData = _Result.ToString();
                    resPlan.Result = ResultType.Success;
                    resPlan.ResultMessage = _Result;
                }
                else
                {
                    resPlan.ResultMessage = "Data Not Found.";
                    resPlan.Result = ResultType.Info;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "editacitvityMaster", "AdminDA", 0, ex.Message.ToString());
                resPlan.ResultMessage = ex.Message.ToString();
                resPlan.Result = ResultType.Error;
            }
            return resPlan;
        }

        public ResultObject<string> editacitvityMasterWithInclude(string RequestType, string param, ActivityMasterData data, ActivitIncludeData data1)
        {
            ResultObject<string> resPlan = new ResultObject<string>();
            try
            {
                var XMLData = XmlConversion.SerializeToXElement(data);
                var XMLDataInc = XmlConversion.SerializeToXElement(data1);
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@XmlInput", XMLData);
                parameters.Add("@XmlInput1", XMLDataInc);
                parameters.Add("@id", param);
                string _Result = SqlMapper.Query<string>(con, "USP_Admin_AcitvityMaster", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (_Result == "Success")
                {
                    resPlan.ResultData = _Result.ToString();
                    resPlan.Result = ResultType.Success;
                    resPlan.ResultMessage = "Success";
                }
                else if (_Result == "Activity Master Title Already Exists")
                {
                    resPlan.ResultData = _Result.ToString();
                    resPlan.Result = ResultType.Success;
                    resPlan.ResultMessage = _Result;
                }
                else
                {
                    resPlan.ResultMessage = "Data Not Found.";
                    resPlan.Result = ResultType.Info;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "editacitvityMasterWithInclude", "AdminDA", 0, ex.Message.ToString());
                resPlan.ResultMessage = ex.Message.ToString();
                resPlan.Result = ResultType.Error;
            }
            return resPlan;
        }

        public ResultObject<List<ActivitIncludeData>> activityInclude(string RequestType, string param1, string param2)
        {
            ResultObject<List<ActivitIncludeData>> resPlan = new ResultObject<List<ActivitIncludeData>>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@param", param1);
                resPlan.ResultData = SqlMapper.Query<ActivitIncludeData>(con, "USP_Admin_AcitvityMaster", param: parameters, commandType: CommandType.StoredProcedure).ToList();

                if (resPlan.ResultData == null || resPlan.ResultData.Count <= 0)
                {
                    resPlan.ResultData = new List<ActivitIncludeData>();
                    resPlan.ResultMessage = "Data Not Found.";
                    resPlan.Result = ResultType.Info;
                }
                else
                {
                    resPlan.ResultMessage = "Success";
                    resPlan.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "activityInclude", "AdminDA", 0, ex.Message.ToString());
                resPlan.ResultMessage = ex.Message.ToString();
                resPlan.Result = ResultType.Error;
            }
            return resPlan;
        }

        public ResultObject<string> editacitvityMasterWithIncludeList(string RequestType, string param, ActivityMasterData data, List<ActivitIncludeData> data1)
        {
            ResultObject<string> resPlan = new ResultObject<string>();
            try
            {
                var XMLData = XmlConversion.SerializeToXElement(data);
                var XMLDataInc = XmlConversion.SerializeToXElement(data1);
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@XmlInput", XMLData);
                parameters.Add("@XmlInput1", XMLDataInc);
                parameters.Add("@id", param);
                //parameters.Add("@masterid", "");
                string _Result = SqlMapper.Query<string>(con, "USP_Admin_AcitvityMaster", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (_Result == "Success")
                {
                    resPlan.ResultData = _Result.ToString();
                    resPlan.Result = ResultType.Success;
                    resPlan.ResultMessage = "Success";
                }
                else if (_Result == "Activity Master Title Already Exists")
                {
                    resPlan.ResultData = _Result.ToString();
                    resPlan.Result = ResultType.Success;
                    resPlan.ResultMessage = _Result;
                }
                else
                {
                    resPlan.ResultMessage = "Data Not Found.";
                    resPlan.Result = ResultType.Info;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "editacitvityMasterWithInclude", "AdminDA", 0, ex.Message.ToString());
                resPlan.ResultMessage = ex.Message.ToString();
                resPlan.Result = ResultType.Error;
            }
            return resPlan;
        }
        public ResultObject<ProfileData> getProfileData(string RequestType, string param1, string param2)
        {
            ResultObject<ProfileData> resQuery = new ResultObject<ProfileData>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@param", param1);
                resQuery.ResultData = SqlMapper.Query<ProfileData>(con, "USP_AdminProfile", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (resQuery.ResultData == null)
                {
                    resQuery.ResultData = new ProfileData();
                    resQuery.ResultMessage = "Data Not Found.";
                    resQuery.Result = ResultType.Info;
                }
                else
                {
                    resQuery.ResultMessage = "Success";
                    resQuery.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "getProfileData", "AdminDA", 0, ex.Message.ToString());
                resQuery.ResultMessage = ex.Message.ToString();
                resQuery.Result = ResultType.Error;
            }
            return resQuery;
        }

        public ResultObject<string> updateProfile(string RequestType, string param1, string param2, ProfileData data)
        {
            ResultObject<string> resPlan = new ResultObject<string>();
            try
            {
                //encrypt
                param1 = Encryption.EncryptString(param1, Encryption.passkey);
                param2 = Encryption.EncryptString(param2, Encryption.passkey);
                var XMLData = XmlConversion.SerializeToXElement(data);
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@XmlInput", XMLData);
                parameters.Add("@password", param1);
                parameters.Add("@param", param2);
                string _Result = SqlMapper.Query<string>(con, "USP_AdminProfile", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (_Result == "Success")
                {
                    resPlan.ResultData = _Result.ToString();
                    resPlan.Result = ResultType.Success;
                    resPlan.ResultMessage = "Success";
                }
                else if (_Result == "The old password entered by you is incorrect")
                {
                    resPlan.ResultData = _Result.ToString();
                    resPlan.Result = ResultType.Success;
                    resPlan.ResultMessage = _Result;
                }
                else
                {
                    resPlan.ResultMessage = "Data Not Found.";
                    resPlan.Result = ResultType.Info;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "updateProfile", "AdminDA", 0, ex.Message.ToString());
                resPlan.ResultMessage = ex.Message.ToString();
                resPlan.Result = ResultType.Error;
            }
            return resPlan;
        }

        public ResultObject<List<TestimonialData>> tetimonial(string RequestType, string param1, string param2)
        {
            ResultObject<List<TestimonialData>> resPlan = new ResultObject<List<TestimonialData>>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                resPlan.ResultData = SqlMapper.Query<TestimonialData>(con, "USP_Admin", param: parameters, commandType: CommandType.StoredProcedure).ToList();

                if (resPlan.ResultData == null || resPlan.ResultData.Count <= 0)
                {
                    resPlan.ResultData = new List<TestimonialData>();
                    resPlan.ResultMessage = "Data Not Found.";
                    resPlan.Result = ResultType.Info;
                }
                else
                {
                    resPlan.ResultMessage = "Success";
                    resPlan.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "tetimonial", "AdminDA", 0, ex.Message.ToString());
                resPlan.ResultMessage = ex.Message.ToString();
                resPlan.Result = ResultType.Error;
            }
            return resPlan;
        }

        public ResultObject<string> edittetimonial(string RequestType, string param, TestimonialData data)
        {
            ResultObject<string> resPlan = new ResultObject<string>();
            try
            {
                var XMLData = XmlConversion.SerializeToXElement(data);
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@XmlInput", XMLData);
                parameters.Add("@id", param);
                string _Result = SqlMapper.Query<string>(con, "USP_Admin", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (_Result == "Success")
                {
                    resPlan.ResultData = _Result.ToString();
                    resPlan.Result = ResultType.Success;
                    resPlan.ResultMessage = "Success";
                }
                else if (_Result == "Testimonial Name Already Exists")
                {
                    resPlan.ResultData = _Result.ToString();
                    resPlan.Result = ResultType.Success;
                    resPlan.ResultMessage = _Result;
                }
                else
                {
                    resPlan.ResultMessage = "Erron an occured";
                    resPlan.Result = ResultType.Info;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "edittetimonial", "AdminDA", 0, ex.Message.ToString());
                resPlan.ResultMessage = ex.Message.ToString();
                resPlan.Result = ResultType.Error;
            }
            return resPlan;
        }

        public ResultObject<string> resetpassword(string RequestType, string param1, string param2)
        {
            ResultObject<string> resPlan = new ResultObject<string>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@email", param1);
                string _Result = SqlMapper.Query<string>(con, "USP_AdminProfile", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (_Result != "0")
                {
                    resPlan.ResultData = _Result.ToString();
                    resPlan.Result = ResultType.Success;
                    resPlan.ResultMessage = "Success";
                }
                else
                {
                    resPlan.ResultMessage = "Erron an occured";
                    resPlan.Result = ResultType.Info;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "edittetimonial", "AdminDA", 0, ex.Message.ToString());
                resPlan.ResultMessage = ex.Message.ToString();
                resPlan.Result = ResultType.Error;
            }
            return resPlan;
        }

        public ResultObject<List<ConfigurationData>> viewConfiguration(string RequestType, string param1, string param2)
        {
            ResultObject<List<ConfigurationData>> resPlan = new ResultObject<List<ConfigurationData>>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                resPlan.ResultData = SqlMapper.Query<ConfigurationData>(con, "USP_Admin", param: parameters, commandType: CommandType.StoredProcedure).ToList();

                if (resPlan.ResultData == null || resPlan.ResultData.Count <= 0)
                {
                    resPlan.ResultData = new List<ConfigurationData>();
                    resPlan.ResultMessage = "Data Not Found.";
                    resPlan.Result = ResultType.Info;
                }
                else
                {
                    resPlan.ResultMessage = "Success";
                    resPlan.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "viewConfiguration", "AdminDA", 0, ex.Message.ToString());
                resPlan.ResultMessage = ex.Message.ToString();
                resPlan.Result = ResultType.Error;
            }
            return resPlan;
        }

        public ResultObject<string> editConfiguration(string RequestType, string param, ConfigurationData data)
        {
            ResultObject<string> resPlan = new ResultObject<string>();
            try
            {
                var XMLData = XmlConversion.SerializeToXElement(data);
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@XmlInput", XMLData);
                string _Result = SqlMapper.Query<string>(con, "USP_Admin", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (_Result == "Success")
                {
                    resPlan.ResultData = _Result.ToString();
                    resPlan.Result = ResultType.Success;
                    resPlan.ResultMessage = "Success";
                }
                else
                {
                    resPlan.ResultMessage = "Erron an occured";
                    resPlan.Result = ResultType.Info;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "editConfiguration", "AdminDA", 0, ex.Message.ToString());
                resPlan.ResultMessage = ex.Message.ToString();
                resPlan.Result = ResultType.Error;
            }
            return resPlan;
        }

        public ResultObject<DashboardData> dashboardData(string RequestType, string param1, string param2)
        {
            ResultObject<DashboardData> resPlan = new ResultObject<DashboardData>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                resPlan.ResultData = SqlMapper.Query<DashboardData>(con, "USP_Admin", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();

                if (resPlan.ResultData == null)
                {
                    resPlan.ResultData = new DashboardData();
                    resPlan.ResultMessage = "Data Not Found.";
                    resPlan.Result = ResultType.Info;
                }
                else
                {
                    resPlan.ResultMessage = "Success";
                    resPlan.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "dashboardData", "AdminDA", 0, ex.Message.ToString());
                resPlan.ResultMessage = ex.Message.ToString();
                resPlan.Result = ResultType.Error;
            }
            return resPlan;
        }

        public ResultObject<List<FaqData>> viewfaq(string RequestType, string param1, string param2)
        {
            ResultObject<List<FaqData>> resPlan = new ResultObject<List<FaqData>>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@id", param1);
                resPlan.ResultData = SqlMapper.Query<FaqData>(con, "USP_Admin", param: parameters, commandType: CommandType.StoredProcedure).ToList();

                if (resPlan.ResultData == null || resPlan.ResultData.Count <= 0)
                {
                    resPlan.ResultData = new List<FaqData>();
                    resPlan.ResultMessage = "Data Not Found.";
                    resPlan.Result = ResultType.Info;
                }
                else
                {
                    resPlan.ResultMessage = "Success";
                    resPlan.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "viewfaq", "AdminDA", 0, ex.Message.ToString());
                resPlan.ResultMessage = ex.Message.ToString();
                resPlan.Result = ResultType.Error;
            }
            return resPlan;
        }

        public ResultObject<string> editfaq(string RequestType, string param, FaqData data)
        {
            ResultObject<string> resPlan = new ResultObject<string>();
            try
            {
                var XMLData = XmlConversion.SerializeToXElement(data);
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@XmlInput", XMLData);
                parameters.Add("@id", param);
                string _Result = SqlMapper.Query<string>(con, "USP_Admin", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (_Result == "Success")
                {
                    resPlan.ResultData = _Result.ToString();
                    resPlan.Result = ResultType.Success;
                    resPlan.ResultMessage = "Success";
                }
                else if (_Result == "Faq question Already Exists")
                {
                    resPlan.ResultData = _Result.ToString();
                    resPlan.Result = ResultType.Success;
                    resPlan.ResultMessage = _Result;
                }
                else
                {
                    resPlan.ResultMessage = "Erron an occured";
                    resPlan.Result = ResultType.Info;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "editfaq", "AdminDA", 0, ex.Message.ToString());
                resPlan.ResultMessage = ex.Message.ToString();
                resPlan.Result = ResultType.Error;
            }
            return resPlan;
        }


        public ResultObject<string> updateresetpass(string RequestType, string param1, string param2, string param3)
        {
            ResultObject<string> resPlan = new ResultObject<string>();
            try
            {
                //encrypt
                param1 = Encryption.EncryptString(param1, Encryption.passkey);
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@password", param1);
                parameters.Add("@param", param2);
                parameters.Add("@param1", param3);
                string _Result = SqlMapper.Query<string>(con, "USP_AdminProfile", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();

                if (RequestType == "resetpasswordUpdate")
                {
                    if (_Result == "Success")
                    {
                        resPlan.ResultData = _Result.ToString();
                        resPlan.Result = ResultType.Success;
                        resPlan.ResultMessage = "Success";
                    }
                    else
                    {
                        resPlan.ResultMessage = "some error Occured";
                        resPlan.Result = ResultType.Info;
                    }
                }
                else
                {
                    if (_Result == "1")
                    {
                        resPlan.ResultData = _Result.ToString();
                        resPlan.Result = ResultType.Success;
                        resPlan.ResultMessage = "Success";
                    }
                    else if (_Result == "0")
                    {
                        resPlan.ResultData = _Result.ToString();
                        resPlan.Result = ResultType.Success;
                        resPlan.ResultMessage = "Success";
                    }
                    else
                    {
                        resPlan.ResultMessage = "some error Occured";
                        resPlan.Result = ResultType.Info;
                    }
                }

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "updateProfile", "AdminDA", 0, ex.Message.ToString());
                resPlan.ResultMessage = ex.Message.ToString();
                resPlan.Result = ResultType.Error;
            }
            return resPlan;
        }
    }
}
